# Functional description

## cfs specification catalog

Service Resolver provides a Catalog function for customer facing service (cfs)
specification

Designer will be able to:

- Declare a new cfs model
- Consult cfs models
- Modify cfs model
- Delete cfs model

A cfs model is usually associated to resource facing service (rfs) models

Some rfs instantiation decision rules can then be associated to cfs–rfs relation (in case
 several solution are possible)

An instantiation rule can be a decision to realize (or not) a rfs instance
at run-time or to find an existing rfs instance.

A cfs model may also be composed of characteristics definitions (=parameters)

### rfs specification catalog

Service Resolver provides a Catalog function for resource facing service
specification (rfs model)

Designer will be able to :

- Declare a new rfs model
- Consult rfs models
- Modify rfs model
- Delete rfs model

Each rfs model relates to a service model pre-defined in ONAP SDC.

Some instantiation rule can be attached to the rfs model, no matter the
cfs model that the rfs will be part of.

A rfs model may also be composed of characteristics definitions (=parameters)
that complete the service model pre-defined in ONAP SDC

### cfs order management

Service Resolver provides cfsOrder API to allow :

- Send a cfsOrder
- Consult cfsOrder

cfsOrder are composed of orderItems
An orderItem is about requesting an operation (add, delete) on a cfs instance.
To add a cfs instance, it is necessary to provide the cfs model

When receiving a cfsOrder, Service Resolver will check the cfsOrder validity

When receiving a « add » orderItem,  Service Resolver will search for all rfs model
that compose the cfs model and send all corresponding rfs order to ONAP NBI.

Before sending rfsOrder, Service Resolver will check if there is any priority
between rfs. Service Resolver will then send all rfsOrder in the right
sequence.

Before sending rfsOrder, Service Resolver will check if there is any condition
to not send the rfsOrder.

### instantiation decision management

Service Resolver provides an instantiation decision API to allow :

process decision (using a POST request message)

based on input data, the API will process a response to provide an instantiation decision

To be able to process the decision, this API server is connected to service inventory API in order to be able to
check&collect existing RFS instances.

Thus, the response will contain the instantiation decison and may also contain:

- a list of existing RFS instances, for example to link a CFS instance to existing RFS instances
- a list of existing related RFS instances, for example to link a new RFS instance to an existing RFS instance

### Demo portal

Service Resolver provides a demo portal to allow :

view cfs instances and related rfs instances
