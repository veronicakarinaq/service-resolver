# Running with docker-compose

pre-requisite : linux, docker, docker compose

 ```bash
 cd
 mkdir projects
 cd ~/projects
 git clone https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver
 cd service-resolver
 docker-compose build
```

if your are behind a proxy you may have to use that kind of command (replace your_proxy)

```bash
docker-compose build --build-arg HTTP_PROXY=<your_proxy> --build-arg HTTPS_PROXY=<your_proxy> api_rfs_spec_server api_service_order_server api_service_spec_server portal api_service_server
docker-compose up
```

all docker images will be built and containers launched

to stop the containers :

docker-compose down

or

docker-compose down -v

the "-v" option will remove the volumes (all stored data are lost)

## Testing all APIs

To check Service Resolver is correctly working, some tests can be runs:
[integration-tests](https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/integration_tests/README.md)

## Simulator mode

By default, Service Resolver is running in "simulator mode" (no need for ONAP)

To run Service Resolver in relation with an ONAP platform, change the "simulator_mode" value from True to False in the config_default.yaml files of "api_rfs_spec_server" and
"api_service_order_server".

## Explore APIs with swaggerUI

Open a web browser and go to

```bash
http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

you should see the swagger UI explorer for API RfsSpecAPI

Open an other tab in your browser and go to

```bash
http://localhost:6002/serviceResolver/serviceOrderManagement/api/v1/ui/
```

you should see the swagger UI explorer for API ServiceOrderAPI

Open an other tab in your browser and go to

```bash
http://localhost:6003/serviceResolver/serviceInventoryManagement/api/v1/ui/
```

you should see the swagger UI explorer for API ServiceAPI

Open an other tab in your browser and go to

```bash
http://localhost:6004/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

you should see the swagger UI explorer for API ServiceSpecAPI

## Graphical Visualisation of service instance

Open an other tab in your browser and go to

```bash
http://localhost:6005/relations/relations_display
```

you should see the demo portal display

## Check communication between Service Resolver and ONAP platforms

Open a web browser and go to

```bash
http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

you should see the swagger UI explorer for API RfsSpecAPI

There is a "GET status" operation available.

click on "status" then "Try It out!" button.

you should see the communication status with each ONAP platform and with the
mongodb database.

In simulation mode, the response is the following:

```json
{
  "communications": {
    "ONAP_platforms": [],
    "get_ONAP_nbi_status": "simulator mode",
    "get_mongodb_status": "ok"
  },
  "name": "rfs_specification_api",
  "status": "ok",
  "version": "v1"
}
```
