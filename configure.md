# configure Service Resolver

each API module has its own configuration file

for the RFS-Spec API :

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_rfs_spec_server/src/app_conf/config_default.yaml
```

for the CFS-Spec API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_service_spec_server/src/app_conf/config_default.yaml
```

for service instance API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_service_server/src/app_conf/config_default.yaml
```

for service order API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_service_order_server/src/app_conf/config_default.yaml
```

for instantiation decision API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_instantiation_decision_server/src/app_conf/config_default.yaml
```

Those files contains

proxy configuration to access ONAP API
simulation mode (true/false)
all URL/Headers infos to access ONAP API
all URL/port infos to access Mongodb API
all URL/port infos to access other Service Resolver API modules
debug level

To run Service Resolver, a docker-compose.yaml file is also used where various hostname, port can be defined

That docker-compose.yaml file is common to all Service Resolver micro-services.
