#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""cfs related operations
"""
import uuid
import logging
import json
import serv_exceptions as service_resolver_exceptions
from service_mongo_client import (mongo_find,
                                  mongo_find_one,
                                  mongo_find_one_and_update,
                                  mongo_delete)
from service_utils import get_config

LOGGER = logging.getLogger('service_api')
SERV_ID = get_config("service_resolver.service_api.id_generated_by_server")
SIMUL = get_config("simulator_mode")


def serviceCreate(Service):  # pylint: disable=C0103
    """create Service instance
    """
    service = Service
    # check if proposed cfs Spec composition is valid
    # based on existing services in ONAP SDC
    query = {'name': service["name"]}
    service["@type"] = "Service"
    service["@baseType"] = "Service"
    service["@schemaLocation"] = ""
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(Service, indent=4, sort_keys=True))
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        # we generate a service id when the application that provides
        # information does not generate the id
        if SERV_ID and SIMUL:
            service['id'] = str(uuid.uuid4())
        if not SIMUL and service["category"] == "cfs":
            service['id'] = str(uuid.uuid4())
        service["href"] = "/service/" + service['id']
        # we insert the service in the database
        query = {'id': service['id']}
        LOGGER.debug('\nInstance up to be inserted in mongo :  %s\n',
                     json.dumps(service, indent=4, sort_keys=True))
        try:
            resp = mongo_find_one_and_update(query, service, True)
        except service_resolver_exceptions.MongodbException:
            LOGGER.error("Unable to connect to Mongodb")
            return {"error": "Unable to connect to Mongodb"}, 503
        return resp, 201
    message_value = "service : " + service["name"] + " already exists"
    message = {"message": message_value}
    LOGGER.debug('message : %s ', message_value)
    return message, 409


def serviceDelete(ServiceId):   # pylint: disable=C0103
    """delete service
    """
    query = {'id': ServiceId}
    try:
        resp = mongo_delete(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def serviceFind():   # pylint: disable=C0103
    """find service
    """
    query = {}
    try:
        resp = mongo_find(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def serviceGet(ServiceId):   # pylint: disable=C0103
    """get service
    """
    query = {'id': ServiceId}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return resp, 200


def serviceUpdate(ServiceId, Service):   # pylint: disable=C0103
    """update service
    """
    query = {'id': ServiceId}
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(Service, indent=4, sort_keys=True))
    try:
        resp = mongo_find_one_and_update(query, Service, False)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return resp, 200
