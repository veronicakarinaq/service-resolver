# Service Resolver for ONAP (SR)

![image1](logo/sr-logo.png)

Application to collect cfsOrders from BSS, process those cfsOrders and generate
 rfsOrders to ONAP-NBI.

## Install and Run

Follow the guide here : [install&run](https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/install_and_run.md)

## Overall Description

Service Resolver is an application that will process service orders.

A service order is a message (conform to TM Forum standard, with extensions) that
contains orderItem. An orderItem is a service (CFS) with an action to be performed
on that service (add, delete).

When the action is "add", Service Resolver will get the service definition
(service "specification" in TM Forum terminology)
in the CFSspec catalog (a database) containing all service definition.

A service definition is a set of data that describes the various "characteristics"
and "solutions" (RFS spec) that can potentially be used to instantiate the service.
For example, to provide a Firewall service, we need a firewall
software solution (RFS spec) based on a software.
There can be multiple firewall software solutions from various vendors or opensource communities.

For each RFS spec, it is possible to indicate which ONAP platform will be
in charge of processing the instantiation. That feature allows to define
and instantiate a customer service that will be built by several "domains",
each having an ONAP system able to instantiate the RFS.

The service (CFS spec) definition contains also some "instantiationRules":
to build a Firewall service we may have choice between several
firewall software or maybe a firewall software is already deployed
and we just have to re-use it (= shared instance notion)

Based on that service (CFS spec) definition, each time a service order is received,
Service Resolver will evaluate the rules and will elaborate
the service "solution": the list of RFS to be instantiated/deployed.

Instantiation Decision, for each RFS, will be processed by the InstantionDecision
API micro service.

For each of those RFS, Service Resolver send an order or request to an ONAP NBI application and that ONAP platform will process the instantiation/deployment of the solution (RFS).

In case of "delete" action, the serviceOrder contains the reference to the instantiated service.
Service Resolver gets the service from its inventory
 to retrieve all components that are in relation with the service instance.

Service Resolver will then check each of those components to see if it can be deleted or not.
The component will be deleted if it is no longer in relation with any other service.

In case, the component is still in use, only the relationship with the service is deleted.

Service Resolver is:

* generic : any kind of service (vFirewall, Internet Access, virtualBox,
  Network Slice service, monitoring service...)
* model-driven : no need for code "per service", only service definition
  (data in database via API)
* adaptative to the context : the service "solution" is evaluated/defined
  at the moment a service order is received, based on rules
* standard API based : TMF641 (serviceOrder), TMF633 (serviceCatalog),
  TMF638 (serviceInventory)
* light : some small (< 100 Mo) docker containers
* usable in simulation mode (without ONAP) to allow Service Designer
  elaborate/test their service definition before deploying the service definition for real.

The solution is composed of six Rest API servers:

* rfs specification API
* cfs specification API
* service (cfs and rfs) instance API
* cfs order API
* instantiation decision API
* instantiation rules inventory API

In addition, a small "demo portal" is also part of service resolver
in order to allow Service Designer to visualize the service instance composition
after having perfomed some serviceOrders

Code is Python based, using "connexion" framework mainly

A common MongoDB database is used to store all information.

Some Postman collection are also provided to help developing some "client" API
application or for testing purpose.
