#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""cfs related operations
"""
import uuid
import logging
import json
import res_exceptions as service_resolver_exceptions
from resource_mongo_client import (mongo_find,
                                   mongo_find_one,
                                   mongo_find_one_and_update,
                                   mongo_delete)
from res_utils import get_config

LOGGER = logging.getLogger('resource_api')
RES_ID = get_config("service_resolver.resource_api.id_generated_by_server")


def resourceCreate(Resource):  # pylint: disable=C0103
    """create Resource instance
    """
    resource = Resource
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(resource, indent=4, sort_keys=True))
    # we chack if resource already exists based on the name
    query = {'name': resource["name"]}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        resource["@type"] = "Resource"
        resource["@baseType"] = "Resource"
        resource["@schemaLocation"] = ""
        # we generate a service id when the application that provides
        # information does not generate the id
        if RES_ID:
            resource['id'] = str(uuid.uuid4())
        resource["href"] = "/resource/" + resource['id']
        # we insert the resource in the database
        query = {'id': resource['id']}
        LOGGER.debug('\nInstance up to be inserted in mongo :  %s\n',
                     json.dumps(resource, indent=4, sort_keys=True))
        try:
            resp = mongo_find_one_and_update(query, resource, True)
        except service_resolver_exceptions.MongodbException:
            LOGGER.error("Unable to connect to Mongodb")
            return {"error": "Unable to connect to Mongodb"}, 503
        return resp, 201
    message_value = "resource : " + resource["name"] + " already exists"
    message = {"message": message_value}
    LOGGER.debug('message : %s ', message_value)
    return message, 409


def resourceDelete(ResourceId):   # pylint: disable=C0103
    """delete resource
    """
    query = {'id': ResourceId}
    try:
        resp = mongo_delete(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def resourceFind():   # pylint: disable=C0103
    """find resource
    """
    query = {}
    try:
        resp = mongo_find(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def resourceGet(ResourceId):   # pylint: disable=C0103
    """get resource
    """
    query = {'id': ResourceId}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return resp, 200


def resourceUpdate(ResourceId, Resource):   # pylint: disable=C0103
    """update resource
    """
    query = {'id': ResourceId}
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(Resource, indent=4, sort_keys=True))
    try:
        resp = mongo_find_one_and_update(query, Resource, False)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return resp, 200
