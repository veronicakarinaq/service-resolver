#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""rfsSpecification related operations
"""

import uuid
import logging
import json
# import yaml
import rfs_spec_exceptions as service_resolver_exceptions
from rfs_spec_client_to_api import get_rfs_spec_from_onap, nbi_healthcheck
from tosca_files_from_sdc import (download_tosca_service_template,
                                  extract_tosca_service_template)
from tosca_to_rfs_spec import tosca_to_rfs_spec_func
from rfs_spec_mongo_client import (mongo_find,
                                   mongo_find_one,
                                   mongo_find_one_and_update,
                                   mongo_delete)
from rfs_spec_utils import get_config

LOGGER = logging.getLogger('rfs_spec_api')


def update_rfs_spec_with_onap_data(rfs_spec):
    """update rfs spec  with Onap data
    """
    onap_platform_name = rfs_spec["supportingResource"][0]["name"]
    LOGGER.error('Trying to connect to ONAP platform: %s', onap_platform_name)
    response = get_rfs_spec_from_onap(rfs_spec["id"], onap_platform_name)
    result = download_tosca_service_template(rfs_spec)
    filename = extract_tosca_service_template(result["path"],
                                              result["csar_filename"])
    rfs_spec_from_tosca = tosca_to_rfs_spec_func(result["path"], filename)
    #     if eleme["distributionStatus"] != "DISTRIBUTED":
    #         response.remove(eleme)
    query = {'id': response['id']}
    mongo_find_one_and_update(query, response, True)
    query = {'id': response['id']}
    mongo_find_one_and_update(query, rfs_spec_from_tosca, True)


def update_data_in_db(rfs_specification):
    """update RfsSpecification in db then return result
    """
    query = {'id': rfs_specification['id']}
    resp = mongo_find_one_and_update(query, rfs_specification, True)
    query = {'id': rfs_specification['id']}
    resp = mongo_find_one(query)
    return resp


def rfsSpecificationCreate(rfsSpecification):  # pylint: disable=C0103
    """create RfsSpecification:
        takes a json with a name, id and optionnal info
        checks if said rfs is defined within ONAP
        if it is, adds it to the db with the optionnal info
    """
    LOGGER.debug('call to rfsSpecificationCreate function')
    simul = get_config("simulator_mode")
    query = {'name': rfsSpecification["name"]}
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(rfsSpecification, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(rfsSpecification))
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is not None:
        message_value = ("rfs_spec : " +
                         rfsSpecification["name"] +
                         " already exists")
        message = {"message": message_value}
        LOGGER.debug('message : %s ', message_value)
        return message, 409
    rfsSpecification["@type"] = "RfsSpecification"
    rfsSpecification["@baseType"] = "ServiceSpecification"
    rfsSpecification["@schemaLocation"] = ""
    rfsSpecification["category"] = "rfs"
    if simul:
        rfsSpecification['id'] = str(uuid.uuid4())
        rfsSpecification['distributionStatus'] = "completed"
    else:
        try:
            update_rfs_spec_with_onap_data(rfsSpecification)
        except (service_resolver_exceptions.MongodbException,
                service_resolver_exceptions.NbiException,
                service_resolver_exceptions.SdcRequestException,
                service_resolver_exceptions.CsarDownloadException,
                service_resolver_exceptions.NbiNotFoundException) as error:
            LOGGER.error("problem : %s", error.args[0])
            message = {"problem": error.args[0]}
            return message, 503
    rfsSpecification["href"] = ("/rfsSpecification/" +
                                rfsSpecification['id'])
    try:
        resp = update_data_in_db(rfsSpecification)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 201


def rfsSpecificationDelete(rfsSpecificationId):  # pylint: disable=C0103
    """delete rfsSpecification:

      takes a rfs id as parameter
        removes said rfs from the db
    """
    LOGGER.debug('call to rfsSpecificationDelete function')
    query = {'id': rfsSpecificationId}
    LOGGER.debug('data received :  %s', rfsSpecificationId)
    try:
        resp = mongo_delete(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def rfsSpecificationFind():  # pylint: disable=C0103
    """find rfsSpecification:

        returns every specification in the database
    """
    LOGGER.debug('call to rfsSpecificationFind function')
    query = {}
    try:
        resp = mongo_find(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def rfsSpecificationGet(rfsSpecificationId):  # pylint: disable=C0103
    """get rfsSpecification:

       takes a rfs id as parameter and returns said rfs
    """
    LOGGER.debug('call to rfsSpecificationGet function')
    query = {'id': rfsSpecificationId}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200


def rfsSpecificationUpdate(rfsSpecification,  # pylint: disable=C0103
                           rfsSpecificationId):  # pylint: disable=C0103
    """update rfsSpecification:
       takes the id of the specifiaction to update
       and a json detailling the changes
    """
    LOGGER.debug('call to rfsSpecificationUpdate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(rfsSpecification, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(rfsSpecification))
    LOGGER.debug('data received :  %s', rfsSpecificationId)
    LOGGER.debug('data type :  %s', type(rfsSpecificationId))

    query = {'id': rfsSpecificationId}
    try:
        resp = mongo_find_one_and_update(query, rfsSpecification, False)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200


def statusFind():  # pylint: disable=C0103
    """find API status:

        returns status
    """
    LOGGER.debug('call to statusFind function')
    simul = get_config("simulator_mode")
    response = {}
    response["name"] = "rfs_specification_api"
    response["status"] = "ok"
    response["version"] = "v1"
    response["communications"] = {}
    response["communications"]["ONAP_platforms"] = []
    platforms = get_config("onap_platforms")
    if not simul:
        for platform in platforms:
            try:
                nbi_healthcheck(platform)
                platform["get_ONAP_nbi_status"] = "ok"
                response["communications"]["ONAP_platforms"].append(platform)
            except service_resolver_exceptions.NbiException:
                LOGGER.error("Unable to connect to NBI")
                response["communications"]["get_ONAP_nbi_status"] = "nok"
                platform["get_ONAP_nbi_status"] = "PROBLEM !!!!!"
                response["communications"]["ONAP_platforms"].append(platform)
                response["status"] = "NOK"
    else:
        response["communications"]["get_ONAP_nbi_status"] = "simulator mode"
        response["status"] = "ok"
    query = {}
    try:
        mongo_find(query)
        response["communications"]["get_mongodb_status"] = "ok"
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        response["communications"]["get_mongodb_status"] = "nok"
        response["status"] = "problem with MongoDB"
    return response, 200
