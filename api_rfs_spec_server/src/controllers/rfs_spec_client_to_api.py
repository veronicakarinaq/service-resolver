#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""clients to other API
"""
import logging
import requests
import rfs_spec_exceptions as service_resolver_exceptions
from rfs_spec_utils import get_config

ONAP_NBI_API_BASE_URL = get_config("onap.nbi.url")
ONAP_NBI_API_HEADER = get_config("onap.nbi.headers")
ONAP_SDC_API_BASE_URL = get_config("onap.sdc.url")
ONAP_SDC_API_DOWNLOAD_HEADER = get_config("onap.sdc.download_headers")
LOGGER = logging.getLogger('rfs_spec_api')


def get_csar_from_onap_sdc(rfs_spec_id, onap_platform_name):
    ''' get a rfs_spec by id
    '''
    LOGGER.debug('call to get_csar_from_onap_sdc function')
    proxies = {}
    platforms = get_config("onap_platforms")
    for platform in platforms:
        if platform["name"] == onap_platform_name:
            proxies = platform["proxies"]
            break
    dl_url = (ONAP_SDC_API_BASE_URL +
              "/sdc/v1/catalog/services/" +
              rfs_spec_id +
              "/toscaModel")
    LOGGER.debug('url used : %s ', dl_url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.get(dl_url,
                                headers=ONAP_SDC_API_DOWNLOAD_HEADER,
                                verify=False,
                                proxies=proxies,
                                stream=True)
    except Exception:
        message = "communication problem with ONAP SDC"
        LOGGER.error(message)
        raise service_resolver_exceptions.SdcRequestException(message)
    if response.status_code == 404:
        message = "csar not found in ONAP SDC"
        LOGGER.error(message)
        raise service_resolver_exceptions.CsarDownloadException(message)
    return response


def get_rfs_spec_from_onap(rfs_spec_id, onap_platform_name):
    ''' get a rfs_spec by id from ONAP NBI catalog
    '''
    LOGGER.debug('call to get_rfs_spec_from_onap function')
    proxies = {}
    platforms = get_config("onap_platforms")
    for platform in platforms:
        if platform["name"] == onap_platform_name:
            proxies = platform["proxies"]
            break
    url = (ONAP_NBI_API_BASE_URL +
           "/serviceSpecification/" +
           rfs_spec_id)
    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.get(url,
                                headers=ONAP_NBI_API_HEADER,
                                proxies=proxies)
    except Exception:
        message = "communication problem with ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiException(message)
    if response.status_code == 404:
        message = "not found in ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiNotFoundException(message)
    response = response.json()
    return response


def nbi_healthcheck(onap_platform):
    ''' nbi_healthcheck
    '''
    LOGGER.debug('call to nbi_healthcheck function')
    proxies = onap_platform["proxies"]
    timeout = get_config("general.timeout")
    url = (ONAP_NBI_API_BASE_URL +
           "/status/")
    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    LOGGER.debug('proxy type :  %s', type(proxies))
    try:
        response = requests.get(url,
                                headers=ONAP_NBI_API_HEADER,
                                proxies=proxies,
                                timeout=timeout)
    except Exception:
        message = "communication problem with ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiException(message)
    response = response.json()
    return response
