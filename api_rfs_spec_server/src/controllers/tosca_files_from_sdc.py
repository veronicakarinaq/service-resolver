#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" tosca to rfs spec operation
"""
import os
import zipfile
import logging
from rfs_spec_client_to_api import get_csar_from_onap_sdc

LOGGER = logging.getLogger('rfs_spec_api')


def download_tosca_service_template(rfs_spec):
    """
        download service CSAR file and save/store service template
    """
    onap_platform_name = rfs_spec["supportingResource"][0]["name"]
    LOGGER.debug('call to download_tosca_service_template')
    response = get_csar_from_onap_sdc(rfs_spec["id"], onap_platform_name)
    path = os.path.dirname(os.path.abspath(__file__)).replace(
        "controllers", "tosca_files_from_onap/")
    csar_filename = "service-" + rfs_spec["name"] + "-csar.csar"
    with open((path + csar_filename), 'wb') as csar_file:
        for chunk in response.iter_content(chunk_size=128):
            csar_file.write(chunk)
    result = {
        "path": path,
        "csar_filename": csar_filename
    }
    return result


def extract_tosca_service_template(path, csar_filename):
    """
        extract service template from CSAR archive
    """
    LOGGER.debug('call to extract_tosca_service_template')
    with zipfile.ZipFile(path + csar_filename) as myzip:
        for name in myzip.namelist():
            if (name[-13:] == "-template.yml" and
                    name[:20] == "Definitions/service-"):
                service_template = name
        with myzip.open(service_template) as file1:
            with open(path + service_template[12:], 'wb') as file2:
                file2.write(file1.read())
    return service_template[12:]
