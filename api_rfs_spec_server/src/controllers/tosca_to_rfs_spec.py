#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" tosca to rfs spec operation
"""
import logging
import yaml

LOGGER = logging.getLogger('rfs_spec_api')


def handle_vnf_vnfm(service_from_sdc):
    """build vnf and vfmodules
    """
    LOGGER.debug('call to handle_vnf_vnfm')
    base = service_from_sdc["topology_template"]["node_templates"]
    for key in base:
        vnfs = []
        vnf = {}
        vnf["name"] = base[key]["metadata"]["name"]
        vnf["id"] = base[key]["metadata"]["UUID"]
        vnf["href"] = ""
        vnf["version"] = ""
        vnf["category"] = "vnf"
        vnf["invariantUUID"] = base[key]["metadata"]["invariantUUID"]
        vnf["customizationUUID"] = (base[key]["metadata"]
                                    ["customizationUUID"])
        vnf["@type"] = "extendedResourceSpecification"
        vnf["@baseType"] = "resourceSpecification"
        vnf["@schemaLocation"] = ""
        vnf["resourceSpecCharacteristic"] = []
        for key_properties in base[key]["properties"]:
            vnf_property = {}
            vnf_property["name"] = key_properties
            vnf_property["default_value"] = (base[key]["properties"]
                                             [key_properties])
            vnf["resourceSpecCharacteristic"].append(vnf_property)
        vnf["resourceSpecRelationship"] = []
        base = service_from_sdc["topology_template"]["groups"]
        for key_vfm in base:
            vf_m = {}
            vnf_name = vnf["name"].lower().replace("_", "")
            vfm_name = (base[key_vfm]
                        ["metadata"]["vfModuleModelName"].lower())
            if vnf_name in vfm_name:
                vf_m["vfModuleModelName"] = (base[key_vfm]["metadata"]
                                             ["vfModuleModelName"])
                tmp = (base[key_vfm]["metadata"]
                       ["vfModuleModelInvariantUUID"])
                vf_m["vfModuleModelInvariantUUID"] = tmp
                tmp = base[key_vfm]["metadata"]["vfModuleModelUUID"]
                vf_m["id"] = tmp
                tmp = (base[key_vfm]
                       ["metadata"]["vfModuleModelCustomizationUUID"])
                vf_m["vfModuleModelCustomizationUUID"] = tmp
                vf_m["category"] = "vf_module"
                vnf["resourceSpecRelationship"].append(vf_m)
        vnfs.append(vnf)
    return vnfs


def tosca_to_rfs_spec_func(path, filename):
    """build rfs spec from SDC service tosca template
    """
    LOGGER.debug('call to tosca_to_rfs_spec_func')
    with open(path + filename) as file:
        service_from_sdc = yaml.safe_load(file)
    rfs_spec_from_tosca = {}
    rfs_spec_from_tosca["name"] = service_from_sdc["metadata"]["name"]
    rfs_spec_from_tosca["id"] = service_from_sdc["metadata"]["UUID"]
    rfs_spec_from_tosca["invariantUUID"] = (service_from_sdc["metadata"]
                                            ["invariantUUID"])
    rfs_spec_from_tosca["resourceSpecification"] = []
    if "node_templates" in service_from_sdc["topology_template"]:
        resp = handle_vnf_vnfm(service_from_sdc)
        rfs_spec_from_tosca["resourceSpecification"] = resp
    return rfs_spec_from_tosca
