#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
""" test tosca to rfs spec operation
"""
import json
from deepdiff import DeepDiff
from tosca_to_rfs_spec import tosca_to_rfs_spec_func


def test_tosca_to_rfs_spec_ok_full():
    """ test tosca to rfs spec operation for full service with VNFs
    """
    input_path = "src/test/data/"
    input_filename = "input_tosca_to_rfs_spec_ok_full.yml"
    result_path = "src/test/data/"
    expected_result_filename = "result_tosca_to_rfs_spec_ok_full.json"
    with open(result_path + expected_result_filename) as file:
        expected_result = json.load(file)
    result = tosca_to_rfs_spec_func(input_path, input_filename)
    print(json.dumps(result, indent=4, sort_keys=True))
    assert DeepDiff(result, expected_result) == {}
