#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""  tests for tosca to rfs spec operation
"""

import requests
from tosca_files_from_sdc import (download_tosca_service_template,
                                  extract_tosca_service_template)


def test_download_tosca_service_template_ok(requests_mock):
    """
        test
    """
    rfs_spec = {"name": "vBoxVNF",
                "id": "5d9d692d-e6ca-4fba-992c-1616593a314e"}
    rfs_spec["supportingResource"] = []
    onap = {"name":"onap_1"}
    rfs_spec["supportingResource"].append(onap)
    expected_result = "service-vBoxVNF-csar.csar"
    dl_url = ("http://sdc.api.be.simpledemo.onap.org:30205" +
              "/sdc/v1/catalog/services/" +
              rfs_spec["id"] +
              "/toscaModel")
    path = "src/test/data/"
    csar_filename = "test.csar"
    with open(path + csar_filename, mode='rb') as file:
        file_content = file.read()
        requests_mock.get(dl_url, content=file_content)
    result = download_tosca_service_template(rfs_spec)
    assert result["csar_filename"] == expected_result


# def test_download_tosca_service_template_connect_timeout(requests_mock):
#     """
#         test
#     """
#     rfs_spec = {"name": "vBoxVNF",
#                 "id": "5d9d692d-e6ca-4fba-992c-1616593a314e"}
#     expected_result = {'error': ''}, 400
#     dl_url = ("http://sdc.api.be.simpledemo.onap.org:30205" +
#               "/sdc/v1/catalog/services/" +
#               rfs_spec["id"] +
#               "/toscaModel")
#     requests_mock.get(dl_url, exc=requests.exceptions.ConnectTimeout)
#     result = download_tosca_service_template(rfs_spec)
#     assert result == expected_result


# def test_download_tosca_service_template_file_not_found(requests_mock):
#     """
#         test
#     """
#     rfs_spec = {"name": "vBoxVNF",
#                 "id": "5d9d692d-e6ca-4fba-992c-1616593a314e"}
#     expected_result = {"error": "file not found in ONAP SDC"}, 404
#     dl_url = ("http://sdc.api.be.simpledemo.onap.org:30205" +
#               "/sdc/v1/catalog/services/" +
#               rfs_spec["id"] +
#               "/toscaModel")
#     requests_mock.get(dl_url, status_code=404)
#     result = download_tosca_service_template(rfs_spec)
#     assert result == expected_result


def test_extract_tosca_service_template():
    """
        test
    """
    path = "src/test/data/"
    csar_filename = "test.csar"
    result = extract_tosca_service_template(path, csar_filename)
    expected_result = "service-Vboxvnf-template.yml"
    assert result == expected_result
