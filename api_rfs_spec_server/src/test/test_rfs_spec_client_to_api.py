#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""  tests for rfs spec client to api operation
"""
import pytest
import requests
from rfs_spec_client_to_api import (get_csar_from_onap_sdc,
                                    get_rfs_spec_from_onap,
                                    nbi_healthcheck)
from rfs_spec_utils import get_config

ONAP_NBI_API_BASE_URL = get_config("onap.nbi.url")
ONAP_NBI_API_HEADER = get_config("onap.nbi.headers")
ONAP_SDC_API_BASE_URL = get_config("onap.sdc.url")
ONAP_SDC_API_DOWNLOAD_HEADER = get_config("onap.sdc.download_headers")


def test_get_csar_from_onap_sdc(requests_mock):
    """
        test
    """
    rfs_spec_id = "0001"
    dl_url = (ONAP_SDC_API_BASE_URL +
              "/sdc/v1/catalog/services/" +
              rfs_spec_id +
              "/toscaModel")
    path = "src/test/data/"
    csar_filename = "test.csar"
    onap_platform_name = "onap_1"
    with open(path + csar_filename, mode='rb') as file:
        file_content = file.read()
        expected_status_code = 200
        requests_mock.get(dl_url, content=file_content)
    response = get_csar_from_onap_sdc(rfs_spec_id, onap_platform_name)
    assert response.status_code == expected_status_code


def test_get_csar_from_onap_sdc_404(requests_mock):
    """
        test
    """
    with pytest.raises(Exception):
        rfs_spec_id = "0001"
        onap_platform_name = "onap_1"
        dl_url = (ONAP_SDC_API_BASE_URL +
                  "/sdc/v1/catalog/services/" +
                  rfs_spec_id +
                  "/toscaModel")
        requests_mock.get(dl_url, status_code=404)
        get_csar_from_onap_sdc(rfs_spec_id, onap_platform_name)


def test_get_csar_from_onap_sdc_communication_problem(requests_mock):
    """
        test
    """
    with pytest.raises(Exception):
        rfs_spec_id = "0001"
        onap_platform_name = "onap_1"
        dl_url = (ONAP_SDC_API_BASE_URL +
                  "/sdc/v1/catalog/services/" +
                  rfs_spec_id +
                  "/toscaModel")
        requests_mock.get(dl_url, exc=requests.exceptions.ConnectTimeout)
        get_csar_from_onap_sdc(rfs_spec_id, onap_platform_name)


def test_get_rfs_spec_from_onap(requests_mock):
    """
        test
    """
    rfs_spec_id = "0001"
    onap_platform_name = "onap_1"
    expected_response = {"key_test": "value_test"}
    mocked_response = {"key_test": "value_test"}
    url = (ONAP_NBI_API_BASE_URL +
           "/serviceSpecification/" +
           rfs_spec_id)
    requests_mock.get(url, json=mocked_response)
    response = get_rfs_spec_from_onap(rfs_spec_id, onap_platform_name)
    assert response == expected_response


def test_get_rfs_spec_from_onap_404(requests_mock):
    """
        test
    """
    with pytest.raises(Exception):
        rfs_spec_id = "0001"
        onap_platform_name = "onap_1"
        url = (ONAP_NBI_API_BASE_URL +
               "/serviceSpecification/" +
               rfs_spec_id)
        requests_mock.get(url, status_code=404)
        get_rfs_spec_from_onap(rfs_spec_id, onap_platform_name)


def test_get_rfs_spec_from_onap_no_response(requests_mock):
    """
        test
    """
    with pytest.raises(Exception):
        rfs_spec_id = "0001"
        onap_platform_name = "onap_1"
        url = (ONAP_NBI_API_BASE_URL +
               "/serviceSpecification/" +
               rfs_spec_id)
        requests_mock.get(url, exc=requests.exceptions.ConnectTimeout)
        get_rfs_spec_from_onap(rfs_spec_id, onap_platform_name)


def test_nbi_healthcheck(requests_mock):
    """
        test
    """
    onap_platform = {
                     "name": "onap_1",
                     "proxies": {}
                    }
    expected_response = {"key_test": "value_test"}
    mocked_response = {"key_test": "value_test"}
    url = (ONAP_NBI_API_BASE_URL +
           "/status/")
    requests_mock.get(url, json=mocked_response)
    response = nbi_healthcheck(onap_platform)
    assert response == expected_response


def test_nbi_healthcheck_no_response(requests_mock):
    """
        test
    """
    with pytest.raises(Exception):
        onap_platform = {
                         "name": "onap_1",
                         "proxies": {}
                        }
        url = (ONAP_NBI_API_BASE_URL +
               "/status/")
        requests_mock.get(url, exc=requests.exceptions.ConnectTimeout)
        nbi_healthcheck(onap_platform)
