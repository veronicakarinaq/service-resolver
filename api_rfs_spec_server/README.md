# RFSspec API server

## Overview

This API server is about providing (read-only) the various service models
defined in ONAP SDC. Those service models are then called resource facing
service specification (so called "rfs specification" from TM Forum wording)

If this API is configured to run with ONAP, each time a request is perform to the API,
there is a request to ONAP SDC.
It is then necessary to have a permanent access to an ONAP platform.

If the API is configured to run in simulation mode, there is no need for ONAP.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in api_rfs_spec_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 api_rfs_catalog_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t api_rfs_spec_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6001:6001 --volume=/data:/data api_rfs_spec_server
```

## Use

open your browser to here:

```bash
http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/swagger.json
```

![screenshot](screenshots/swagger-1.PNG)
