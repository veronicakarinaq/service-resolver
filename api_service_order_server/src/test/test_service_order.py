#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    test file for sorderification_controler
"""
import json
import pytest
from service_order_controller import (cfsOrderCreate,
                                      cfsOrderDelete,
                                      cfsOrderFind,
                                      cfsOrderGet,
                                      cfsOrderUpdate) # pylint: disable=C0103


# @pytest.mark.parametrize("post_rfs_order_message, expected_final_status_code, expected_final_response_file", [
#     ("all rfs order taken into account by ONAP", 201, "service_order_vbox_with_rfs_orders.json" ),
#     ("a rfs_spec_list", 400, "error12.json")
# ])
# def test_cfsOrderCreate_all_order_item_are_valid(mocker,
#                                                  post_rfs_order_message,
#                                                  expected_final_status_code,
#                                                  expected_final_response_file):
#     '''test
#     '''
#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)

#     expected_final_response = service_order

#     cfs_spec_list = [{"id": "140646745461600",
#                       "name": "vBox_FixedAccess",
#                       "version": "1"}]

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'build_cfs_spec_list'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=cfs_spec_list)

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'all_order_item_are_valid'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[])

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'service_instance_declare'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[{"id":"00"}, 201])

#     test_data_path = "src/test/data/"
#     result_filename = "solution_vbox.json"
#     with open(test_data_path + result_filename) as file:
#         solution = json.load(file)

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'build_solution'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=solution)

#     input_filename = "service_order_vbox_with_rfs_orders.json"
#     with open(test_data_path + input_filename) as file:
#         process_rfs_orders_service_order_result = json.load(file)

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'process_rfs_orders'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[post_rfs_order_message,
#                                process_rfs_orders_service_order_result])

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'declare_cfs_rfs_relationship'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'rfs_delete_orders'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[{}, 200])

#     input_filename = expected_final_response_file
#     with open(test_data_path + input_filename) as file:
#         expected_final_response = json.load(file)

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value={})

#     response, status_code = cfsOrderCreate(service_order)
#     assert status_code == expected_final_status_code
#     assert response == expected_final_response


# def test_cfsOrderCreate_not_all_order_item_are_valid(mocker):
#     '''test
#     '''
#     ERROR11 = {"code": "11",
#                "message": "those cfs_spec do not exist : vBox_FixedAccess",
#                "description": ""}
#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)

#     input_filename = "error11.json"
#     with open(test_data_path + input_filename) as file:
#         expected_response = json.load(file)

#     expected_status_code = 400

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'build_cfs_spec_list'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[])

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'all_order_item_are_valid'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=["vBox_FixedAccess"])

#     response, status_code = cfsOrderCreate(service_order)
#     assert status_code == expected_status_code
#     assert response == expected_response


# def test_cfsOrderDelete(mocker):
#     '''test
#     '''
#     cfs_order_id = ""
#     expected_response = {}
#     expected_status_code = 204

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'delete_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock), return_value={})

#     response, status_code = cfsOrderDelete(cfs_order_id)
#     assert status_code == expected_status_code
#     assert response == expected_response


# @pytest.mark.parametrize("rfs_order_file", [
#     ("rfs_order_completed.json"),
#     ("rfs_order_rejected.json")
# ])
# def test_cfsOrderFind(mocker, rfs_order_file):
#     '''test
#     '''
#     expected_response = []

#     test_data_path = "src/test/data/"
#     input_filename = "service_order_list.json"
#     with open(test_data_path + input_filename) as file:
#         service_order_list = json.load(file)

#     input_filename = rfs_order_file
#     with open(test_data_path + input_filename) as file:
#         rfs_order = json.load(file)

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=service_order_list)

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'get_rfs_order'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=rfs_order)

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value={})

#     response = cfsOrderFind()
#     response = []
#     assert response == expected_response


# @pytest.mark.parametrize("rfs_order_file", [
#     ("rfs_order_completed.json"),
#     ("rfs_order_rejected.json")
# ])
# def test_cfsOrderGet(mocker, rfs_order_file):
#     '''test
#     '''
#     cfs_order_id = "0000"
#     expected_response = []

#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)

#     input_filename = rfs_order_file
#     with open(test_data_path + input_filename) as file:
#         rfs_order = json.load(file)

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=service_order)

#     func_where_is_func_to_mock = 'service_order_controller'
#     func_to_mock = 'get_rfs_order'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=rfs_order)

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value={})

#     response = cfsOrderGet(cfs_order_id)
#     response = []
#     assert response == expected_response


# def test_cfsOrderUpdate(mocker):
#     '''test
#     '''
#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)
#     cfs_order_id = ""
    # expected_response = service_order
    # expected_status_code = 200

    # func_where_is_func_to_mock = 'pymongo.collection.Collection'
    # func_to_mock = 'find_one_and_update'
    # mocker.patch((func_where_is_func_to_mock + "." + func_to_mock), return_value={})

    # response, status_code = cfsOrderUpdate(cfs_order_id, service_order)
    # assert status_code == expected_status_code
    # assert response == expected_response
