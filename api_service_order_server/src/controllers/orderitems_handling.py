#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""orderItems related operations
"""
import logging
from sorder_client_to_api import get_service_spec

LOGGER = logging.getLogger('service_order_api')


def service_order_linting(service_order):
    """check cfsOrder
    """
    # test_data_path = "src/templates/"
    # input_filename = "order_schema.json"
    # with open(test_data_path + input_filename) as file:
    #     try:
    #         schema = json.load(file)
    #         jsonschema.validate(service_order, schema)
    #     except Exception as valid_err:
    #         print("Validation KO: {}".format(valid_err))
    #         raise valid_err

    # get all cfs that are ordered
    for elem in service_order["orderItem"]:
        if elem["action"] == "add":
            get_service_spec(elem["service"]["serviceSpecification"]["id"])


def check_charac(spec, order):
    """
        checking the validity of characteristics
    """
    verif = False
    for elem in spec["serviceSpecCharacteristic"]:
        for ite in order["service"]["serviceCharacteristic"]:
            if elem["name"] == ite["name"]:
                val = elem["value"]
                if type_check(val, ite["valueType"]):
                    verif = True
    return verif


def type_check(val, typ):
    """
        checking type depending on string
    """
    verif = False
    if typ == "string":
        verif = isinstance(val, str)
    elif typ == "boolean":
        verif = isinstance(val, bool)
    elif typ == "number":
        verif = isinstance(val, (int, float))
    return verif
