#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""select rfs that need to be instantiated based on decision_rules
"""
import logging
import json
from operator import itemgetter
# from decision_operations import perform_decision
from sorder_client_to_api import (get_service_spec,
                                  obtain_decision,
                                  get_rfs_spec)

LOGGER = logging.getLogger('service_order_api')


def process_case_no_rule(solution, onap_pf, rfs_spec):
    """
        case when no decision rule to process
    """
    rfs_spec_infos = {}
    rfs_spec_infos["id"] = rfs_spec["id"]
    rfs_spec_infos["name"] = rfs_spec["name"]
    # rfs_spec_infos["href"] = rfs_spec["href"]
    tmp = rfs_spec["instantiationPriority"]
    rfs_spec_infos["instantiationPriority"] = tmp
    # here after is the case when there is an alloted resource
    if "serviceSpecRelationship" in rfs_spec:
        tmp = rfs_spec["serviceSpecRelationship"]
        rfs_spec_infos["serviceSpecRelationship"] = tmp
    # here after the ONAP platform that will be used to instantiate
    # the rfs
    rfs_spec_infos["supportingResource"] = onap_pf
    solution["rfs_spec_list"].append(rfs_spec_infos)
    return solution


def process_rfs_spec_list(rfs_spec_list, order_item, solution):
    """
        process rfs_spec_list
    """
    tmp_existing_rfs = []
    for rfs_spec in rfs_spec_list:
        full_rfs_spec = get_rfs_spec(rfs_spec["id"])
        onap_pf = full_rfs_spec["supportingResource"]
        LOGGER.debug('\nStudy rfs_spec :  %s\n',
                     json.dumps(rfs_spec, indent=4, sort_keys=True))
        decision_list = sorted(rfs_spec["instantiationDecisionRules"],
                               key=itemgetter('ruleEvaluationPriority'))
        if not decision_list:  # case when there is no instantiation rule
            LOGGER.debug('\nno rule provided')
            decision = "instantiate"
            solution = process_case_no_rule(solution, onap_pf, rfs_spec)
            continue  # next rfs
        # here after there is at least a rule to evaluate
        finished = False
        rule = decision_list[0]
        while not finished:
            LOGGER.debug('\nRule to evaluate :  %s\n',
                         rule["rule_name"])
            decision = obtain_decision(order_item,
                                       rule["rule_params"],
                                       rule["rule_results"],
                                       rule["rule_name"])
            LOGGER.debug('\nDecision :  %s\n',
                         json.dumps(decision, indent=4, sort_keys=True))
            if decision["decision"]["is_definitive_decision"]:
                finished = True
            else:
                tmp = decision["decision"]
                next_rule = tmp["next_instantiationDecisionRules"] - 1
                rule = decision_list[next_rule]
        # decision is known now
        if decision["decision"]["decision"] == "instantiate":
            simplified_rfs_spec = {}
            simplified_rfs_spec["id"] = rfs_spec["id"]
            simplified_rfs_spec["name"] = rfs_spec["name"]
            if "serviceSpecRelationship" in rfs_spec:
                tmp = rfs_spec["serviceSpecRelationship"]
                simplified_rfs_spec["serviceSpecRelationship"] = tmp
            # simplified_rfs_spec["href"] = rfs_spec["href"]
            tmp = rfs_spec["instantiationPriority"]
            simplified_rfs_spec["instantiationPriority"] = tmp
            simplified_rfs_spec["supportingResource"] = onap_pf
            solution["rfs_spec_list"].append(simplified_rfs_spec)
        # in case we have some existing rfs to take into account
        if decision["rfs"] != []:
            tmp_existing_rfs.append(decision["rfs"])
        if decision["related_rfs"] != []:
            for elem in decision["related_rfs"]:
                solution["existing_rfs"].append(elem)
    return solution, tmp_existing_rfs


def build_solution(order_item):
    """
        get service definition based on requested order_item
        run all decisionRules associated to the service definition
        obtain the list of service components to order, with some additional
        parameters
    """
    # initialize solution
    solution = {}
    solution["rfs_spec_list"] = []
    solution["existing_rfs"] = []
    # get the cfs spec to obtain the list of potentials rfs spec
    tmp = order_item["service"]["serviceSpecification"]["id"]
    service_spec = get_service_spec(tmp)
    LOGGER.debug('Elaborating solution for cfs :  %s :',
                 order_item["service"]["name"])
    # we sort the rfs_spec by instantiation priority
    rfs_spec_list = sorted(service_spec["serviceSpecRelationship"],
                           key=itemgetter('instantiationPriority'))
    solution, tmp_existing_rfs = process_rfs_spec_list(rfs_spec_list,
                                                       order_item,
                                                       solution)
    # we build the data for the existing rfs instances
    for rfs in tmp_existing_rfs:
        simplified_rfs = {}
        simplified_rfs["id"] = rfs["id"]
        simplified_rfs["name"] = rfs["name"]
        simplified_rfs["href"] = rfs["href"]
        simplified_rfs["spec_name"] = rfs["serviceSpecification"]["name"]
        simplified_rfs["spec_id"] = rfs["serviceSpecification"]["id"]
        solution["existing_rfs"].append(simplified_rfs)
    # we build a list with unique rfs instances
    tmp = list({v['id']: v for v in solution["existing_rfs"]}.values())
    solution["existing_rfs"] = tmp
    return solution
