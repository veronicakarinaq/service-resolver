#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""rfs specification related operations
"""
import os
import logging
import json
import uuid
from flask import render_template
from service_handling import service_instance_declare
from sorder_client_to_api import (delete_service,
                                  get_service,
                                  update_service,
                                  send_order_to_nbi)
from sorder_mongo_client import mongo_find_one_and_update
from sorder_utils import get_config

LOGGER = logging.getLogger('service_order_api')
SIMUL = get_config("simulator_mode")


def process_rfs_orders_one_by_one(order_item, solution, service_order):
    """process all rfs_order from solution
    """
    for rfs_spec in solution["rfs_spec_list"]:
        # build rfsOrder that will be sent to NBI
        rfs_order = {}
        related_party = {}
        related_party["id"] = service_order["relatedParty"][0]["id"]
        related_party["name"] = service_order["relatedParty"][0]["name"]
        related_party["role"] = service_order["relatedParty"][0]["role"]
        order_item["service"]["relatedParty"] = []
        order_item["service"]["relatedParty"].append(related_party)
        # order_rfs(rfsSpec) via NBI
        response = post_rfs_order(rfs_spec,
                                  order_item,
                                  SIMUL)
        # update rfs order with informations coming from NBI response
        rfs_order["id"] = response["id"]
        rfs_order["href"] = response["href"]
        rfs_order["ordered_rfs_spec_id"] = rfs_spec["id"]
        rfs_order["ordered_rfs_spec_name"] = rfs_spec["name"]
        if "serviceSpecRelationship" in rfs_spec:
            tmp = rfs_spec["serviceSpecRelationship"]
            rfs_order["ordered_rfs_spec_relationships"] = tmp
        rfs_order["requested_action"] = response["orderItem"][0]["action"]
        rfs_order["order_item_state"] = response["orderItem"][0]["state"]
        rfs_order["@type"] = "extendedOrderItemRelationship"
        rfs_order["@baseType"] = "OrderItemRelationship"
        # here after is the rfs instance id
        serv_id = response["orderItem"][0]["service"]["id"]
        rfs_order["rfs_instance_id"] = serv_id
        # here after is the rfs instance name
        name = response["orderItem"][0]["service"]["name"]
        rfs_order["rfs_instance_name"] = name
        rfs_order["relationshipType"] = "cfsOrder_to_rfsOrder"
        # update cfs_order with rfs_order
        if "orderItemRelationship" not in order_item:
            order_item["orderItemRelationship"] = []
        order_item["orderItemRelationship"].append(rfs_order)
        relation = order_item["orderItemRelationship"]
        service_order["orderItem"][0]["orderItemRelationship"] = relation
    # here after we handle the state of each cfs, based of rfs state
    service_order["orderItem"][0]["state"] = "completed"
    # LOGGER.debug('\nservice_order :  %s\n',
    #              json.dumps(service_order, indent=4, sort_keys=True))
    if "orderItemRelationship" in service_order:
        for elem in service_order["orderItem"][0]["orderItemRelationship"]:
            if elem["order_item_state"] != "completed":
                tmp = elem["order_item_state"]
                service_order["orderItem"][0]["state"] = tmp
                break
    # here after we handle the service order state
    service_order["state"] = "completed"
    for elem in service_order["orderItem"]:
        if service_order["orderItem"][0]["state"] != "completed":
            service_order["state"] = service_order["orderItem"][0]["state"]
            break
    return service_order


def simul_post_order(order_item, rfs_spec):
    """in case of simul mode
    """
    if order_item["action"] == "add":
        # here after we handle the "add" action
        test_data_path = os.path.dirname(os.path.abspath(__file__))
        test_data_path = test_data_path + "/data/"
        input_filename = "get_rfs_order_simu_response_add_completed.json"
        with open(test_data_path + input_filename) as file:
            mock = json.load(file)
        # in SIMUL mode we generate all IDs
        mock["id"] = str(uuid.uuid4())
        mock["href"] = ("serviceOrder/" + mock["id"])
        mock["orderItem"][0]["service"]["id"] = str(uuid.uuid4())
        name = rfs_spec["name"] + "_" + str(uuid.uuid4())
        mock["orderItem"][0]["service"]["name"] = name

        specid = mock["orderItem"][0]["service"]["serviceSpecification"]["id"]
        specid = rfs_spec["id"]
        mock["orderItem"][0]["service"]["serviceSpecification"]["id"] = specid

        name = mock["orderItem"][0]["service"]["serviceSpecification"]["name"]
        name = rfs_spec["name"]
        mock["orderItem"][0]["service"]["serviceSpecification"]["name"] = name

        related_party = {}
        related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
        party_name = order_item["service"]["relatedParty"][0]["name"]
        related_party["name"] = party_name
        party_role = order_item["service"]["relatedParty"][0]["role"]
        related_party["role"] = party_role
        party = mock["orderItem"][0]["service"]["relatedParty"]
        party.append(related_party)
        mock["orderItem"][0]["service"]["relatedParty"] = party
        item = mock["orderItem"][0]
        LOGGER.debug('RFS MOCK %s :', mock)
        service = service_instance_declare(item,
                                           mock,
                                           "rfs")
        mock["orderItem"][0]["service"]["id"] = service["id"]
        LOGGER.debug('\nMOCK response RFS ORDER :\n%s\n',
                     json.dumps(mock, indent=4, sort_keys=True))
        return mock
    # here after we handle the "delete" action
    test_data_path = os.path.dirname(os.path.abspath(__file__))
    test_data_path = test_data_path + "/data/"
    input_filename = "get_rfs_order_simu_response_delete_completed.json"
    with open(test_data_path + input_filename) as file:
        mock = json.load(file)
    mock["id"] = str(uuid.uuid4())
    mock["href"] = ("serviceOrder/" + mock["id"])
    mock["orderItem"][0]["service"]["id"] = order_item["service"]["id"]
    mock["orderItem"][0]["service"]["name"] = order_item["service"]["name"]
    delete_service(mock["orderItem"][0]["id"])
    return mock


def real_post_order(order_item, rfs_spec, payload):
    """in case of simul mode
    """
    if order_item["action"] == "add":
        # here after we handle the "add" action
        name = (payload["orderItem"][0]["service"]["name"] + "_" +
                str(uuid.uuid4()))
        payload["orderItem"][0]["service"]["name"] = name
        onap_platform_name = rfs_spec["supportingResource"][0]["name"]
        response = send_order_to_nbi(payload, onap_platform_name)
        rfs_order_resp = response.copy()
        # here after we copy the rfs instance in Service Resolver
        # note that ONAP provides all the IDs
        related_party = {}
        related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
        party_name = order_item["service"]["relatedParty"][0]["name"]
        related_party["name"] = party_name
        party_role = order_item["service"]["relatedParty"][0]["role"]
        related_party["role"] = party_role
        party = []
        party.append(related_party)
        rfs_order_resp["orderItem"][0]["service"]["relatedParty"] = party

        item = rfs_order_resp["orderItem"][0]
        item["service"]["@type"] = "Service"
        item["service"]["@schemaLocation"] = ""
        item["service"]["@baseType"] = "Service"
        item["service"]["href"] = ""
        item["service"]["supportingResource"] = []
        # here after we add the onap platform that support that instance
        tmp = rfs_spec["supportingResource"][0]
        # item["service"]["supportingResource"][0] = tmp
        item["service"]["supportingResource"].append(tmp)
        item["service"]["serviceCharacteristic"] = []
        item["service"]["serviceSpecification"]["@type"] = ""
        item["service"]["serviceSpecification"]["@schemaLocation"] = ""
        item["service"]["serviceSpecification"]["@baseType"] = ""
        item["service"]["serviceSpecification"]["href"] = ""
        item["service"]["serviceSpecification"]["name"] = ""
        item["service"]["serviceSpecification"]["targetServiceSchema"] = ""
        item["service"]["serviceSpecification"]["version"] = ""
        service_instance_declare(item,
                                 rfs_order_resp,
                                 "rfs")
        return rfs_order_resp
    # here after we handle the "delete" action
    payload["id"] = str(uuid.uuid4())
    payload["href"] = ("serviceOrder/" + payload["id"])
    # related party information
    related_party = {}
    related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
    party_name = order_item["service"]["relatedParty"][0]["name"]
    related_party["name"] = party_name
    party_role = order_item["service"]["relatedParty"][0]["role"]
    related_party["role"] = party_role
    party = []
    party.append(related_party)
    payload["relatedParty"] = party
    # order item
    payload["orderItem"][0]["service"]["id"] = order_item["service"]["id"]
    payload["orderItem"][0]["service"]["name"] = order_item["service"]["name"]
    # onap platform name
    onap_platform_name = rfs_spec["supportingResource"][0]["name"]
    # service specification
    payload["orderItem"][0]["service"]["serviceSpecification"]["id"] = rfs_spec["serviceSpecification"]["id"]
    response = send_order_to_nbi(payload, onap_platform_name)
    delete_service(payload["orderItem"][0]["id"])
    return response


def post_rfs_order(rfs_spec,
                   order_item,
                   simul):
    """send service Order to ONAP NBI
    """
    if order_item["action"] == "add":
        payload_template = 'rfs_order_payload_alacarte.json'
    elif order_item["action"] == "delete":
        payload_template = 'rfs_order_payload_delete.json'
    body_string = render_template(
        payload_template,
        rfs_spec=rfs_spec,
        order_item=order_item)
    payload = json.loads(body_string)
    # here after we have the switch between simulator mode or real mode
    if not simul:
        rfs_order_resp = real_post_order(order_item, rfs_spec, payload)
        return rfs_order_resp
    mock = simul_post_order(order_item, rfs_spec)
    return mock


def rfs_full_delete(cfs,
                    rfs,
                    rfs_spec,
                    rfs_order_item,
                    service_order):
    """full rfs delete via rfsOrder
    """
    # there is no other cfs related to the rfs, we send delete order
    response = post_rfs_order(rfs_spec,
                              rfs_order_item,
                              SIMUL)
    # update rfs order with informations coming from NBI response
    rfs_order = {}
    rfs_order["id"] = response["id"]
    rfs_order["href"] = response["href"]
    rfs_order["action"] = response["orderItem"][0]["action"]
    rfs_order["state"] = response["state"]
    rfs_order["relationshipType"] = "cfsOrder_to_rfsOrder"
    # update cfs_order with the new rfs_order
    if "orderItemRelationship" not in rfs_order_item:
        rfs_order_item["orderItemRelationship"] = []
    rfs_order_item["orderItemRelationship"].append(rfs_order)
    relation = rfs_order_item["orderItemRelationship"]
    service_order["orderItem"][0]["orderItemRelationship"] = relation
    # DEPRECATED service_order["orderRelationship"].append(rfs_order)
    # store cfs_order in database
    query = {'id': service_order['id']}
    mongo_find_one_and_update(query, service_order, True)
    # remove rfs relationship in cfs
    cfs["supportingService"].remove(rfs)
    # update cfs in service_api
    get_service(cfs["id"])
    update_service(cfs["id"], cfs)

    # delete rfs in service_API
    delete_service(rfs["id"])
    return cfs


def rfs_delete_orders(order_item, service_order):
    """process all rfs_order delete except if the rfs instance is in use
       with multiple cfs (we check the number of cfs relationships)
    """
    cfs_id = order_item["service"]["id"]
    # request to service API
    response = get_service(cfs_id)
    cfs = response
    rfs_list = cfs["supportingService"].copy()
    for rfs in rfs_list:
        # request to service API
        response = get_service(rfs["id"])
        stored_rfs = response
        # rfs_spec = stored_rfs["serviceSpecification"]
        rfs_order_item = order_item.copy()
        rfs_order_item["service"] = stored_rfs
        if len(stored_rfs["serviceRelationship"]) == 1:
            # there is no other cfs related to the rfs, we send delete order
            cfs = rfs_full_delete(cfs,
                                  rfs,
                                  stored_rfs,
                                  rfs_order_item,
                                  service_order)
        else:
            # the rfs in use by multiple cfs_spec
            # we only delete the relationships : no delete rfs operation
            #
            # find an remove the relationship in rfs
            for elem in stored_rfs["serviceRelationship"]:
                if elem["id"] == cfs["id"]:
                    stored_rfs["serviceRelationship"].remove(elem)
                    break
            # request to update in service API
            response = update_service(stored_rfs["id"], stored_rfs)
            # remove rfs relationship in cfs
            cfs["supportingService"].remove(rfs)
    # request to update in service API
    response = update_service(cfs["id"], cfs)
    return service_order
