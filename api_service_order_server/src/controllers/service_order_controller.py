#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""cfs order related operations
"""
import uuid
import logging
import json
from datetime import datetime
import pytz
# import jsonschema
import so_exceptions as so_excepts
from orderitems_handling import service_order_linting
from service_order_actions import (add_action_processing,
                                   delete_action_processing)
from sorder_mongo_client import (mongo_find,
                                 mongo_find_one,
                                 mongo_find_one_and_update,
                                 mongo_delete)
from sorder_utils import get_config

LOGGER = logging.getLogger('service_order_api')
ORDER_ID = get_config("service_resolver.service_order_api" +
                      ".id_generated_by_server")


def cfsOrderCreate(CfsOrder):  # pylint: disable=C0103
    """create ServiceOrder
    """
    LOGGER.debug('call to cfsOrderCreate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(CfsOrder, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(CfsOrder))
    service_order = CfsOrder
    service_order["@type"] = "CfsOrder"
    service_order["@baseType"] = "ServiceOrder"
    service_order["@schemaLocation"] = ""
    utc = pytz.utc
    now_utc = datetime.now(utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
    service_order["orderDate"] = now_utc
    try:
        service_order_linting(service_order)
    except (so_excepts.MongodbException,
            so_excepts.RfsSpecException,
            so_excepts.RfsSpecNotFoundException,
            so_excepts.CfsSpecException,
            so_excepts.CfsSpecNotFoundException) as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return message, 503

    customer = service_order["relatedParty"]
    for order_item in service_order["orderItem"]:
        # generate an Id for the serviceOrder
        if ORDER_ID:
            service_order['id'] = str(uuid.uuid4())
        service_order["href"] = "/cfsOrder/" + service_order['id']
        service_order['state'] = 'acknowledged'
        if order_item["action"] == "add":
            try:
                service_order = add_action_processing(order_item,
                                                      service_order,
                                                      customer)
            except (so_excepts.MongodbException,
                    so_excepts.NbiException,
                    so_excepts.SdcRequestException,
                    so_excepts.NbiNotFoundException,
                    so_excepts.NbiServiceIdException,
                    so_excepts.ServiceException) as error:
                LOGGER.error("problem : %s", error.args[0])
                message = {"problem": error.args[0]}
                return message, 503
        if order_item["action"] == "delete":
            try:
                service_order = delete_action_processing(order_item,
                                                         service_order)
            except (so_excepts.MongodbException,
                    so_excepts.NbiException,
                    so_excepts.SdcRequestException,
                    so_excepts.NbiNotFoundException,
                    so_excepts.ServiceNotFoundException) as error:
                LOGGER.error("problem : %s", error.args[0])
                message = {"problem": error.args[0]}
                return message, 503
        # store serviceOrder in database
        query = {'id': service_order['id']}
        try:
            mongo_find_one_and_update(query, service_order, True)
        except (so_excepts.MongodbException) as error:
            LOGGER.error("problem : %s", error.args[0])
            message = {"problem": error.args[0]}
            return message, 503
    return service_order, 201


def cfsOrderDelete(cfsOrderId):  # pylint: disable=C0103
    """delete ServiceOrder
        only for maintenance purpose
    """
    LOGGER.debug('call to cfsOrderDelete function')

    query = {'id': cfsOrderId}
    try:
        resp = mongo_delete(query)
    except so_excepts.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def cfsOrderFind():  # pylint: disable=C0103
    """find ServiceOrder
       during the find operation, we update cfs order with all rfs order
    """
    LOGGER.debug('call to cfsOrderFind function')

    # search service_orders which are in a not completed state
    # service_order_list = list(sorder.find({"state": {"$ne": "completed"}},
    #                                       {'_id': False}))
    query = {}
    try:
        resp = mongo_find(query)
    except so_excepts.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def cfsOrderGet(cfsOrderId):  # pylint: disable=C0103
    """get ServiceOrder
    """
    LOGGER.debug('call to cfsOrderGet function')

    # search for service_order
    query = {'id': cfsOrderId}
    try:
        resp = mongo_find_one(query)
    except so_excepts.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200


def cfsOrderUpdate(cfsOrderId, CfsOrder):  # pylint: disable=C0103
    """update ServiceOrder
    """
    LOGGER.debug('call to cfsOrderUpdate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(CfsOrder, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(CfsOrder))
    service_order_id = cfsOrderId
    service_order = CfsOrder
    query = {'id': service_order_id}
    try:
        resp = mongo_find_one_and_update(query, service_order, False)
    except so_excepts.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200
