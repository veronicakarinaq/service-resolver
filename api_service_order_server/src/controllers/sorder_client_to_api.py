#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""clients to other API
"""
import logging
import json
from time import sleep
import requests
import so_exceptions as service_resolver_exceptions
from sorder_utils import get_config

LOGGER = logging.getLogger('service_order_api')
SERVICE_API_BASE_URL = ("http://" +
                        get_config("service_resolver.service_api.host") +
                        ":" +
                        str(get_config("service_resolver.service_api.port")) +
                        get_config("service_resolver.service_api.url"))
SERVICE_API_HEADER = get_config("service_resolver.service_api.headers")
SERVICE_SPEC_API_BASE_URL = ("http://" +
                             get_config("service_resolver.cfs_spec_api.host") +
                             ":" +
                             str(get_config("service_resolver" +
                                            ".cfs_spec_api.port")) +
                             get_config("service_resolver.cfs_spec_api.url"))
SERVICE_SPEC_API_HEADER = get_config("service_resolver.cfs_spec_api.headers")
RFS_SPEC_API_BASE_URL = ("http://" +
                         get_config("service_resolver.rfs_spec_api.host") +
                         ":" +
                         str(get_config("service_resolver" +
                                        ".rfs_spec_api.port")) +
                         get_config("service_resolver.rfs_spec_api.url"))
RFS_SPEC_API_HEADER = get_config("service_resolver.rfs_spec_api.headers")
ONAP_NBI_URL = get_config("onap.nbi.url")
ONAP_NBI_URL_HEADER = get_config("onap.nbi.headers")


def get_rfs_order_from_nbi(rfs_order_id, onap_platform_name):
    """get service Order from ONAP NBI
    """
    LOGGER.debug('call to get_rfs_order_from_nbi function')
    proxies = {}
    platforms = get_config("onap_platforms")
    for platform in platforms:
        if platform["name"] == onap_platform_name:
            proxies = platform["proxies"]
            break
    url_sorder_by_id = ONAP_NBI_URL + "/serviceOrder/" + rfs_order_id

    LOGGER.debug('url used : %s ', url_sorder_by_id)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url_sorder_by_id,
                                    headers=ONAP_NBI_URL_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiException(message)
    if response.status_code == 404:
        message = "not found in ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiNotFoundException(message)
    response = response.json()
    return response


def send_order_to_nbi(payload, onap_platform_name):
    """sending orders to ONAP NBI
    """
    LOGGER.debug('call to send_order_to_nbi function')
    proxies = {}
    platforms = get_config("onap_platforms")
    for platform in platforms:
        if platform["name"] == onap_platform_name:
            proxies = platform["proxies"]
            break
    url_sorder = ONAP_NBI_URL + "/serviceOrder"
    LOGGER.debug('url used : %s ', url_sorder)
    LOGGER.debug('proxy used : %s ', proxies)
    LOGGER.debug('\nPayload in rfsOrder sent to NBI :\n%s\n',
                 json.dumps(payload, indent=4, sort_keys=True))
    response = {}
    try:
        response = requests.request("POST",
                                    url_sorder,
                                    headers=ONAP_NBI_URL_HEADER,
                                    proxies=proxies,
                                    json=payload)
    except Exception:
        message = "communication problem with ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiException(message)
    if response.status_code == 404:
        message = "not found in ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiNotFoundException(message)
    response = response.json()
    LOGGER.debug('\nrfsOrder response from NBI :\n%s\n',
                 json.dumps(response, indent=4, sort_keys=True))
    nb_try = 5
    while nb_try > -1:
        LOGGER.debug('TRY GET SERVICE : %s ', nb_try)
        rfs_order = get_rfs_order_from_nbi(response["id"], onap_platform_name)
        LOGGER.debug('RFS-ORDER from NBI :\n%s\n',
                     json.dumps(rfs_order, indent=4, sort_keys=True))
        if rfs_order["orderItem"][0]["state"] == "completed":
            return rfs_order
        if rfs_order["orderItem"][0]["state"] == "rejected":
            return rfs_order
        if rfs_order["orderItem"][0]["state"] == "failed":
            return rfs_order
        nb_try = nb_try - 1
        sleep(3)
    if rfs_order["orderItem"][0]["service"]["id"] is None:
        message = "Service instance Id was not returned by ONAP NBI"
        LOGGER.error(message)
        raise service_resolver_exceptions.NbiServiceIdException(message)
    return response


def get_service_spec(service_spec_id):
    ''' get a service by id
    '''
    LOGGER.debug('call to get_service_spec function')

    url = (SERVICE_SPEC_API_BASE_URL +
           '/serviceSpecification/' +
           service_spec_id)
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=SERVICE_SPEC_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with CFS SPEC API"
        LOGGER.error(message)
        raise service_resolver_exceptions.CfsSpecException(message)
    if response.status_code == 404:
        message = "not found in cfs spec API"
        LOGGER.error(message)
        raise service_resolver_exceptions.CfsSpecNotFoundException(message)
    response = response.json()
    return response


def create_service(service):
    ''' create a service
    '''
    LOGGER.debug('call to create_service function')

    url = SERVICE_API_BASE_URL + "/service"
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("POST",
                                    url,
                                    json=service,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    if response.status_code == 409:
        message = response.json()
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    response = response.json()
    LOGGER.debug('\nCREATE_SERVICE response from service API :\n%s\n',
                 json.dumps(response, indent=4, sort_keys=True))
    return response


def get_service(service_id):
    ''' get a service by id
    '''
    LOGGER.debug('call to get_service function')

    url = (SERVICE_API_BASE_URL +
           '/service/' +
           service_id)
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    if response.status_code == 404:
        message = "not found in service API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceNotFoundException(message)
    response = response.json()
    return response


def find_service():
    ''' find service
    '''
    LOGGER.debug('call to find_service function')

    url = SERVICE_API_BASE_URL + "/service"
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    response = response.json()
    return response


def update_service(service_id, service):
    ''' change a service
    '''
    LOGGER.debug('call to update_service function')

    url = (SERVICE_API_BASE_URL +
           '/service/' +
           service_id)
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("PUT",
                                    url,
                                    json=service,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    response = response.json()
    return response


def delete_service(service_id):
    ''' delete a service
    '''
    LOGGER.debug('call to delete_service function')

    url = (SERVICE_API_BASE_URL +
           '/service/' +
           service_id)
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("DELETE",
                                    url,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    response = {}
    return response


def obtain_decision(order_item, rule_params, rule_results, rule_name):
    ''' ask to process an instantiation decision
        and get decision result in the response
    '''
    LOGGER.debug('call to obtain_decision function')
    host_key = "rule_executor." + rule_name + ".host"
    LOGGER.debug('host_key used : %s ', host_key)

    host = get_config(host_key)
    LOGGER.debug('host used : %s ', host)

    decision_api_base_header = get_config("rule_executor." +
                                          rule_name +
                                          ".headers")
    decision_api_base_url = ("http://" +
                             get_config("rule_executor." +
                                        rule_name + ".host") +
                             ":" +
                             str(get_config("rule_executor." +
                                            rule_name + ".port")) +
                             get_config("rule_executor." +
                                        rule_name + ".url"))

    url = decision_api_base_url + "/instantiationDecision"

    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    decision_request_info = {}
    decision_request_info["orderItem"] = order_item
    decision_request_info["rule_name"] = rule_name
    decision_request_info["rule_params"] = rule_params
    decision_request_info["rule_results"] = rule_results
    LOGGER.debug('\ndata to be provided to Decision API:\n%s\n',
                 json.dumps(decision_request_info, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(decision_request_info))
    try:
        response = requests.request("POST",
                                    url,
                                    json=decision_request_info,
                                    headers=decision_api_base_header,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with DECISION API"
        LOGGER.error(message)
        raise service_resolver_exceptions.DecisionException(message)
    if response.status_code == 409:
        message = response.json()
        LOGGER.error(message)
        raise service_resolver_exceptions.DecisionException(message)
    response = response.json()
    return response


def get_rfs_spec(rfs_spec_id):
    ''' get a rfs spec by id
    '''
    LOGGER.debug('call to get_rfs_spec function')

    url = (RFS_SPEC_API_BASE_URL +
           '/rfsSpecification/' +
           rfs_spec_id)
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=RFS_SPEC_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with RFS SPEC API"
        LOGGER.error(message)
        raise service_resolver_exceptions.RfsSpecException(message)
    if response.status_code == 404:
        message = "not found in rfs spec API"
        LOGGER.error(message)
        raise service_resolver_exceptions.RfsSpecNotFoundException(message)
    response = response.json()
    return response
