#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""add service operations
"""

import logging
import json
from service_handling import (service_instance_declare,
                              declare_cfs_rfs_relationship)
from sorder_client_to_api import delete_service
from build_solution import build_solution
from rfs_order import process_rfs_orders_one_by_one, rfs_delete_orders

LOGGER = logging.getLogger('service_order_api')


def delete_action_processing(order_item, service_order):
    """delete service operations
    """
    service_order["solution"] = {}
    service_order = rfs_delete_orders(order_item,
                                      service_order)
    delete_service(order_item["service"]["id"])
    return service_order


def add_action_processing(order_item, service_order, customer):
    """add service operations
    """
    # update the related party at service_level
    order_item["service"]["relatedParty"] = customer
    # declare a service instance
    service = service_instance_declare(order_item,
                                       service_order,
                                       "cfs")
    # update the service instance id and href in service_order
    # to be able to provide a complete response
    order_item["service"]["id"] = service["id"]
    order_item["service"]["href"] = "service/"+service["id"]

    # calculate the deployment flavor to be instantiated
    solution = build_solution(order_item)
    LOGGER.debug('SOLUTION %s :',
                 json.dumps(solution, indent=4, sort_keys=True))
    order_item["solution"] = solution
    order_item["@type"] = "extendedOrderItem"
    order_item["@baseType"] = "OrderItem"
    # generate all rfsOrder
    service_order = process_rfs_orders_one_by_one(order_item,
                                                  solution,
                                                  service_order)
    existing_rfs = solution["existing_rfs"]
    declare_cfs_rfs_relationship(service_order,
                                 service,
                                 existing_rfs)
    return service_order
