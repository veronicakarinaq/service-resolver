#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""service (cfs) instance related operations
"""
import logging
import json
import uuid
from sorder_client_to_api import (get_service,
                                  update_service,
                                  create_service,
                                  get_service_spec)
from sorder_utils import get_config

LOGGER = logging.getLogger('service_order_api')
SIMUL = get_config("simulator_mode")


def service_instance_declare(order_item, service_order, category):
    """create service instance
    """
    # add information about cfs or rfs order to the service instance
    service = order_item["service"].copy()
    if category == "cfs":
        cfs_spec = get_service_spec(service["serviceSpecification"]["id"])
        service["serviceSpecification"]["href"] = cfs_spec["href"]
        service["id"] = str(uuid.uuid4())
    service["serviceOrder"] = []
    service["category"] = category
    s_order = {}
    s_order["id"] = service_order["id"]
    s_order["orderDate"] = service_order["orderDate"]
    s_order["action"] = order_item["action"]
    s_order["href"] = "/cfsOrder/" + s_order["id"]
    service["serviceOrder"].append(s_order)

    # initialize the serviceRelationship array where we will put informations
    # about rfs instances
    service["serviceRelationship"] = []
    service["supportingService"] = []
    # create the service instance via the API
    LOGGER.debug('\nCFS or RFS instance to be sent to service API :  %s\n',
                 json.dumps(service, indent=4, sort_keys=True))
    LOGGER.debug('\ndata type :  %s\n', type(service))
    response = create_service(service)
    service = response
    return service


def build_rfs_list(service_order, existing_rfs_list):
    """ rfs list related to a cfs
    """
    rfs_list = []
    # LOGGER.debug('\nservice_order in build_rfs_list :  %s\n',
    #              json.dumps(service_order, indent=4, sort_keys=True))
    if "orderItemRelationship" in service_order["orderItem"][0]:
        tmp = service_order["orderItem"][0]["orderItemRelationship"]
        for rfs_order in tmp:
            rfs = {}
            rfs["id"] = rfs_order["rfs_instance_id"]
            rfs["name"] = rfs_order["rfs_instance_name"]
            rfs["spec_name"] = rfs_order["ordered_rfs_spec_name"]
            rfs["spec_id"] = rfs_order["ordered_rfs_spec_id"]
            rfs["href"] = rfs_order["href"]
            if "ordered_rfs_spec_relationships" in rfs_order:
                tmp = rfs_order["ordered_rfs_spec_relationships"]
                rfs["ordered_rfs_spec_relationships"] = tmp
            rfs_list.append(rfs)
    for existing_rfs in existing_rfs_list:
        rfs = {}
        rfs["id"] = existing_rfs["id"]
        rfs["name"] = existing_rfs["name"]
        rfs["spec_name"] = existing_rfs["spec_name"]
        rfs["spec_id"] = existing_rfs["spec_id"]
        rfs["href"] = existing_rfs["href"]
        rfs_list.append(rfs)
    return rfs_list


def declare_cfs_rfs_relationship(service_order, service, existing_rfs):
    """ declare in rfs relationship to cfs instance
    """
    rfs_list = build_rfs_list(service_order, existing_rfs)
    rfs_list2 = rfs_list.copy()
    # get cfs instance stored infos from service API
    response = get_service(service["id"])
    new_cfs = response.copy()
    for rfs in rfs_list:
        # rfs infos to be declared in the cfs instance
        rfs_infos = {}
        rfs_infos["id"] = rfs["id"]
        rfs_infos["name"] = rfs["name"]
        rfs_infos["spec_name"] = rfs["spec_name"]
        rfs_infos["href"] = rfs["href"]
        new_cfs["supportingService"].append(rfs_infos)

        # cfs infos to be declared in each rfs instance
        service_infos = {}
        service_infos["id"] = service["id"]
        service_infos["name"] = service["name"]
        service_infos["href"] = service["href"]
        spec_name = service["serviceSpecification"]["name"]
        service_infos["spec_name"] = spec_name
        # if SIMUL:
        #  rfs update via service API
        response = get_service(rfs["id"])
        new_rfs = response.copy()
        if "reliesOnRfs" not in new_rfs:
            new_rfs["reliesOnRfs"] = []
        # rfs infos to be declared in each rfs instance when
        # there is a reliesOn (alloted resource)
        if "ordered_rfs_spec_relationships" in rfs:
            for elem in rfs["ordered_rfs_spec_relationships"]:
                for elem2 in rfs_list2:
                    if elem["id"] == elem2["spec_id"]:
                        relies_on_rfs = {}
                        relies_on_rfs["id"] = elem2["id"]
                        relies_on_rfs["name"] = elem2["name"]
                        new_rfs["reliesOnRfs"].append(relies_on_rfs)
        new_rfs["serviceRelationship"].append(service_infos)
        response = update_service(new_rfs["id"], new_rfs)

        # cfs update via service API
        response = update_service(new_cfs["id"], new_cfs)
