#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests spec_utils
"""
import os
import pytest
from rule_inventory_utils import get_parameter_from_yaml, get_config


def test_get_parameter_from_yaml():
    ''' test
    '''
    parameter = "mongodb.host"
    expected_value = "mongodb"
    local_path = os.path.dirname(os.path.abspath(__file__))
    yaml_ = local_path.replace(
        "test", "test/data/test_config_default.yaml")
    value = get_parameter_from_yaml(parameter, yaml_)
    assert value == expected_value


def test_get_parameter_from_yaml_not_defined():
    ''' test
    '''
    parameter = "mongodb.toto"
    local_path = os.path.dirname(os.path.abspath(__file__))
    yaml_ = local_path.replace(
        "test", "test/data/test_config_default.yaml")
    with pytest.raises(ValueError, match="Parameter mongodb.toto not defined"):
        assert get_parameter_from_yaml(parameter, yaml_)


def test_get_config(mocker):
    ''' test
    '''
    parameter = "mongodb.host"
    expected_value = "mongodb"
    value = get_config(parameter)
    assert value == expected_value
