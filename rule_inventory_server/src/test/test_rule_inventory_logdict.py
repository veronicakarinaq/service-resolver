#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests logdict
"""
from rule_inventory_logdict import conf_dict


def test_conf_dict():
    ''' test
    '''
    filen = ""
    expected_response = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default': {
                'level': 'DEBUG',
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',  # Default is stderr
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'standard',
                'filename': filen
                }
        },
        'loggers': {
            'rule_inventory_api': {
                'handlers': ['default', 'file'],
                'level': 'DEBUG',
                'propagate': True
            }
        }
    }
    response = conf_dict(filen)
    assert response == expected_response
