# instantiation rule inventory API server

## Overview

This API server is about providing a rule inventory.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in rule_inventory_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 rule_inventory_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t rule_inventory_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6007:6007 --volume=/data:/data rule_inventory_server
```

## Use

open your browser to here:

```bash
http://localhost:6007/serviceResolver/instantiationRuleManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6007/serviceResolver/instantiationRuleManagement/api/v1/swagger.json
```
