#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""handling mongo communications
"""
import logging
import os
from pymongo import MongoClient
from pymongo.collection import ReturnDocument
import cfs_spec_exceptions as service_resolver_exceptions

MONGODB_HOST = os.environ['mongo_server_hostname']
MONGODB_PORT = int(os.environ['mongo_server_port'])
CLIENT = MongoClient(MONGODB_HOST, MONGODB_PORT)
DATABASE = CLIENT['Onap']
CFS_SPEC = DATABASE.cfs_spec
LOGGER = logging.getLogger('cfs_spec_api')


def mongo_find_one(query):
    """find one in mongo
    """
    try:
        response = CFS_SPEC.find_one(query, {'_id': False})
    except Exception:
        message = "problem with MongoDB communication"
        LOGGER.error(message)
        raise service_resolver_exceptions.MongodbException(message)
    return response


def mongo_find_one_and_update(query, new_thing, upsert):
    """find and update in mongo
    """
    try:
        CFS_SPEC.create_index('id', unique=True)
        option = ReturnDocument.AFTER
        response = CFS_SPEC.find_one_and_update(query,
                                                {'$set': new_thing},
                                                {'_id': False},
                                                upsert=upsert,
                                                return_document=option)
    except Exception:
        message = "problem with MongoDB communication"
        LOGGER.error(message)
        raise service_resolver_exceptions.MongodbException(message)
    return response


def mongo_delete(query):
    """delete in mongo
    """
    try:
        CFS_SPEC.delete_one(query)
    except Exception:
        message = "problem with MongoDB communication"
        LOGGER.error(message)
        raise service_resolver_exceptions.MongodbException(message)
    return {}


def mongo_find(query):
    """find in mongo
    """
    try:
        response = list(CFS_SPEC.find(query, {'_id': False}))
    except Exception:
        message = "problem with MongoDB communication"
        LOGGER.error(message)
        raise service_resolver_exceptions.MongodbException(message)
    return response
