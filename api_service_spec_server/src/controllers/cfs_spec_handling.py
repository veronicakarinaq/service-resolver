#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""cfs Specification related operations
"""
import os
import logging
import yaml
import cfs_spec_exceptions as sr_exceptions
from cfs_spec_client_to_api import get_rfs_spec
from cfs_spec_utils import get_config

LOGGER = logging.getLogger('cfs_spec_api')
SIMUL = get_config("simulator_mode")


def check_cfs_spec(service_spec):
    """check if cfs Spec composition is valid
    """
    bad_rfs = {}
    message = {}
    message["bad_rfs_list"] = []
    message["message"] = "OK"
    characs = dict()
    for param in service_spec["serviceSpecCharacteristic"]:
        if "possible_values" in param:
            characs[param["name"]] = param["possible_values"]
        else:
            characs[param["name"]] = "blank"
    # here, we made the hypothesis that all
    # serviceSpecRelationship are RFS spec
    for elem in service_spec["serviceSpecRelationship"]:
        check_param(elem, characs)
        response = get_rfs_spec(elem)
        # if response.status_code != 206
        # or elem["distributionStatus"] != "DISTRIBUTED":
        if response == {}:
            bad_rfs["id"] = elem["id"]
            bad_rfs["name"] = elem["name"]
            bad_rfs["href"] = elem["href"]
            bad_rfs["status"] = "not in rfs_spec catalog"
            message["bad_rfs_list"].append(bad_rfs)
            bad_rfs = {}
            raise sr_exceptions.RfsSpecNotFoundException(message)
        if not SIMUL:
            rfs_spec = response
            if rfs_spec["distributionStatus"] != "DISTRIBUTED":
                bad_rfs["id"] = elem["id"]
                bad_rfs["name"] = elem["name"]
                bad_rfs["href"] = elem["href"]
                bad_rfs["status"] = "not Distributed"
                message["bad_rfs_list"].append(bad_rfs)
                bad_rfs = {}
                # TEMPORARY DISABLED : rfs_list_validated = False
                raise sr_exceptions.RfsSpecNotDistributedException(message)
            elem["href"] = rfs_spec["href"]
            elem["@type"] = "CfsSpecRelationship"
            elem["@baseType"] = "ServiceSpecRelationship"
            elem["@schemaLocation "] = ""
        elem["category"] = response["category"]
        if "serviceSpecRelationship" in response:
            tmp = response["serviceSpecRelationship"]
            elem["serviceSpecRelationship"] = tmp
    return service_spec


def generate_tosca(service_spec):
    """generate tosca file
    """
    path = os.path.dirname(os.path.abspath(__file__)).replace(
        "controllers", "tosca/")
    filename = 'cfs-' + service_spec["name"] + '-template.yml'
    with open(path + filename, 'w+') as file:
        yaml.dump(service_spec, file, default_flow_style=False)


def check_param(elem, characs):
    """
        check characteristics of spec
    """
    message = ""
    for rule in elem["instantiationDecisionRules"]:
        if "cfs_param_name" in rule["rule_params"]:
            parame = rule["rule_params"]["cfs_param_name"]
            if parame not in characs.keys():
                message = ("rule param name does not match" +
                           "any cfs characteristic name")
                LOGGER.error(message)
                raise sr_exceptions.IncorrectRuleParamsException(message)
            if characs[parame] != "blank":
                if rule["rule_params"]["target_value"] not in characs[parame]:
                    message = ("param value does not match" +
                               "any cfs characteristic value")
                    LOGGER.error(message)
                    raise sr_exceptions.IncorrectTargetValueException(message)
    return True
