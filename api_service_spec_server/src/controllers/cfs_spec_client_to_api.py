#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""clients to other API
"""
import logging
import requests
import cfs_spec_exceptions as service_resolver_exceptions
from cfs_spec_utils import get_config

LOGGER = logging.getLogger('cfs_spec_api')
PORT = str(get_config("service_resolver.rfs_spec_api.port"))
RFS_SPEC_API_BASE_URL = ("http://" +
                         get_config("service_resolver.rfs_spec_api.host") +
                         ":" +
                         PORT +
                         get_config("service_resolver.rfs_spec_api.url"))
RFS_SPEC_API_HEADER = get_config("service_resolver.rfs_spec_api.headers")


def get_rfs_spec(rfs_spec):
    ''' get a rfs_spec by id
    '''
    LOGGER.debug('call to get_rfs_spec function')

    url = RFS_SPEC_API_BASE_URL + '/rfsSpecification/' + rfs_spec["id"]
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=RFS_SPEC_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with RFS SPEC API"
        LOGGER.error(message)
        raise service_resolver_exceptions.RfsSpecException(message)
    if response.status_code == 404:
        message = (rfs_spec["name"] +
                   " with id " +
                   rfs_spec["id"] +
                   " not found in rfs spec API")
        LOGGER.error(message)
        raise service_resolver_exceptions.RfsSpecNotFoundException(message)
    response = response.json()
    return response
