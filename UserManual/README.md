# overview

 Service Designer will need to define rfs models and cfs models.

Using Postman (for example), he will have the possibility to load those models into Service Resolver via Rest APIs

After those operations, the Service Designer will need to test those models.

In order to test, he will have to define some service orders.

Using Postman (for example), he will have the possibilité to send (POST) those orders to Service Resolver.

Using the service resolver portal, he will see the result : instanciated services (cfs and rfs) with there relationships

A service API is also available to consult cfs and rfs instances.
