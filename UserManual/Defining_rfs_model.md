# Basic RFS Modeling concepts

A rfs model is a set of information that describe a service usually defined ONAP SDC

the rfs model data structure will then be send via a POST message to Service Resolver using the rfs spec API.

if using Service Resolver in simulation mode, only one line is usually necessary to define a rfs model

```json
{
  "name": "vFW_model_A"
}
```

if using Service Resolver connected with some ONAP platforms, it is necessary
to add the "id" and the ONAP platform that will perform the instantiation:

```json
{
  "name": "vFW_model_A",
  "id": "{{auto_vFW_model_A_spec_id}}",
  "supportingResource": [
    {"name": "onap_1",
    "id": "001",
    "role": [
          {
          "description": "NFV-Orchestrator",
          "id": "001",
          "name": "NFV-Orchestrator"
          }
        ]
    }
  ]
}
```

the id value have to match with a service model UUID from ONAP SDC catalog.
the response will contain the full service definition with all informations coming from ONAP

the "name" of the ONAP platform have to match with the platform informations located in the
API server configuration file [see](https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_rfs_spec_server/src/app_conf/config_default.yaml)

Currently, hypothesis is that each ONAP platform is directly accessible or via a socks5 proxy
and that connectivity between service resolver and ONAP platforms is pre-established.

A curl example of the POST request:

```bash
curl -X POST \
  http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/rfsSpecification \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "name": "vFW_model_B",
  "id": "2ce45a41-b6ca-46e6-81a7-8458cc821e3e"
}
'
```

A curl example of the GET request :

```bash
curl -X GET \
  http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/rfsSpecification \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache'
'
```

You may prefer to use the API explorer (swaggerUI) : on your web browser go to

```bash
http://localhost:6001/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

You should see a web page give you access to all API operation about RfsSpecification resource

## Alloted Resource concept from ONAP

In ONAP, there is the concept of "alloted resource". Despite the naming, it is in fact a service that
is supported by an other service (for example : a vRF in a vRouter).

In case of alloted resource, it is possible to complete the basic rfs model definition :

```json
{
  "name": "BRG",
  "id": "{{auto_BRG_spec_id}}",
    "serviceSpecRelationship": [
        {
            "id": "{{auto_BRG_Emulator_spec_id}}",
            "name": "BRG_Emulator",
            "relationshipType": "reliesOn",
            "description": "BRG_Emulator is an allotted resource linked to  BRG"
        }
        ]
}
```

Be careful in the sequencyng : you have to declare those two RFS in the right sequence.

In the example above, you have to declare "BRG_Emulator" before "BRG" because you need the "id" value of BRG_Emulator to be able to declare BRG.
