# Demo Portal

Demo portal is used to visualized the instantiated services (cfs and rfs) with relationship

The purpose is to allow Service Designer to check if the result after some orders is in conformance with what expected.

It is also used for Service Resolver demos

The user need to POST service orders and then refesh the portal page to see the result

Some screens

![image1](sreenshots/vFW_1.PNG)

![image2](sreenshots/vFW_2.PNG)

![image3](sreenshots/vFW_3.PNG)
