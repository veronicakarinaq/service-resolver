# Define a service order

A service order is a set of information that describe an order to add/delete an orderItem (usually describing a customer service)

the service order  data structure will then be send via a POST message to Service Resolver using the service order API

```json
http://api_service_order_server:6002/serviceResolver/serviceOrderManagement/api/v1/cfsOrder
```

A service order will describe the orderItem, the related customer (if any) the service parameter values, the action we want to perform (add or delete)

In case of "add" action, it is necessary to indicate the service model to be used

In case of "delete" action, it is necessary to indicate the service instance id.

Here is after an example to order to "add" vFirewall service for customer (relatedParty) JohnDoe

```json
{
  "category": "vFW service",
  "description": "service order for vFW customer service",
  "externalId": "BSS_order_01",
   "note": {
    "author": "Orange",
    "date": "May 2019",
    "text": ""
  },
  "notificationContact": "",
  "orderItem": [
    {
      "action": "add",
      "id": "1",
      "service": {
        "name": "vFW_01",
        "place": [
          {
            "id": "",
            "name": "",
            "role": ""
          }
        ],
        "relatedParty": [
          {
            "id": "JohnDoe",
            "name": "JohnDoe",
            "role": "customer",
            "href": ""
          }
        ],
        "serviceCharacteristic": [
          {
            "name": "featureLevel",
            "value": "simple"
          }
        ],
        "serviceSpecification": {
          "id": "{{auto_vFW_cfs_spec_id}}",
          "name": "vFW",
          "version": "1"
        },
        "serviceState": "active",
        "serviceType": ""
      }
    }
  ],
  "priority": "1",
  "relatedParty": [
    {
      "id": "JohnDoe",
      "name": "JohnDoe",
      "role": "customer",
      "href": ""
    }
  ]
}
```

and here after an example pour a "delete" action :

```json
{
  "category": "vFW service",
  "description": "service order for vFW customer service",
  "externalId": "Customer_01",
   "note": {
    "author": "Orange",
    "date": "May 2019",
    "text": ""
  },
  "notificationContact": "",
  "orderItem": [
    {
      "action": "delete",
      "id": "1",
      "service": {
        "name": "vFW_01",
        "id": "{{auto_vFW_cfs_01_id}}",
        "place": [
          {
            "id": "",
            "name": "",
            "role": ""
          }
        ],
        "relatedParty": [
          {
            "id": "JohnDoe",
            "name": "JohnDoe",
            "role": "customer",
            "href": ""
          }
        ]
       }
    }
  ],
  "priority": "1",
  "relatedParty": [
    {
      "id": "JohnDoe",
      "name": "JohnDoe",
      "role": "customer",
      "href": ""
    }
  ]
}

```
