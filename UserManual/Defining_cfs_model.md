# CFS model definition basis

A cfs model is a set of information that describe a service usually defined from customer point of view

the cfs model data structure will then be send via a POST message to Service Resolver using the cfs spec API.

```json
http://localhost:6004/serviceResolver/serviceCatalogManagement/api/v1/serviceSpecification
```

cfs model data structure contains:

* a name and description of the cfs model
* a list of key/values coming from TMForum standard
* a serviceSpecCharacteristic array with all the service parameter definitions
* a serviceSpecRelationship array with all the rfs models that can compose the cfs

for each element in the serviceSpecRelationship array, there is :

* a name and id of the rfs model (MUST match with existing RfsSpecification from RFSspec catalog)
* a relationship type
* a description
* an instantiationPriority with a value that will provide a clue to Service Resolver to know about sequencing the instanciation of rfs
* an instantiationDecisionRules array

An example

```json
{
    "description": "vFW use-case",
    "name": "vFW",
    "version": "1.0",
    "category": "cfs",
    "href": "",
    "lastUpdate": "",
    "lifecycleStatus": "",
    "@type": "",
    "@baseType": "",
    "validFor": {
      "startDateTime": "",
     "endDateTme": ""
    },
    "serviceSpecCharacteristic": [
        {
            "description": "",
            "name": "featureLevel",
            "valueType": "string",
            "possible_values": ["simple", "rich"],
            "mandatory_characteristic": true
        }
    ],
    "serviceSpecRelationship": [
        {
            "id": "{{auto_vFW_model_A_spec_id}}",
            "name": "vFW_model_A",
            "relationshipType": "cfs_to_rfs",
            "instantiationPriority": 1,
            "description": "that rfs is instantiated by default (= featureLevel is not rich)",
            "instantiationDecisionRules": [
            {
                "ruleEvaluationPriority": 1,
                "rule_name": "param_in_cfs_order_rule",
                "rule_params": {
                    "cfs_param_name": "featureLevel",
                    "operator": "equal",
                    "target_value": "simple"
                },
                "rule_results": [{
                        "rule_response": "no_param",
                        "decision": "instantiate",
                        "is_definitive_decision": true
                    },
                    {
                        "rule_response": "true",
                        "decision": "instantiate",
                        "is_definitive_decision": true
                    },
                    {
                        "rule_response": "false",
                        "decision": "do_not_instantiate",
                        "is_definitive_decision": true
                    }
                ]
            }
            ]
        },
        {
            "id": "{{auto_vFW_model_B_spec_id}}",
            "name": "vFW_model_B",
            "relationshipType": "cfs_to_rfs",
            "instantiationPriority": 1,
            "description": "that rfs is instantiated only if featureLevel is rich and if there is no longer capacity on existing ",
            "instantiationDecisionRules": [
            {
                "ruleEvaluationPriority": 1,
                "rule_name": "param_in_cfs_order_rule",
                "rule_params": {
                    "cfs_param_name": "featureLevel",
                    "operator": "equal",
                    "target_value": "rich"
                },
                "rule_results": [{
                        "rule_response": "no_param",
                        "decision": "do_not_instantiate",
                        "is_definitive_decision": true
                    },
                    {
                        "rule_response": "true",
                        "decision": "instantiate",
                        "is_definitive_decision": false,
                        "next_instantiationDecisionRules": 2
                    },
                    {
                        "rule_response": "false",
                        "decision": "do_not_instantiate",
                        "is_definitive_decision": true
                    }
                ]
            },
                {
                    "ruleEvaluationPriority": 2,
                    "rule_name": "rfs_instance_capacity_rule",
                    "rule_params": {
                        "spec_name": "vFW_model_B",
                        "set_id_key": "all",
                        "capacity": 2
                    },
                    "rule_results": [{
                            "rule_response": "true",
                            "decision": "do_not_instantiate",
                            "is_definitive_decision": true
                        },
                        {
                            "rule_response": "false",
                            "decision": "instantiate",
                            "is_definitive_decision": true
                        }
                    ]
                }

            ]
        }
    ]
}
```

A curl example of the GET request :

```bash
curl -X GET \
  http://localhost:6004/serviceResolver/serviceCatalogManagement/api/v1/rfsSpecification \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache'
'
```

You may prefer to use the API explorer (swaggerUI) : on your web browser go to

```bash
http://localhost:6004/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

You should see a web page give you access to all API operation about CfsSpecification resource

## Instantiation Rules

Service Resolver proposes 3 rules

* param_in_cfs_order_rule  : the rfs will be instantiated based if a parameter exists in the order with a specific value
* rfs_instance_in_a_set_rule : the rfs will be instantiated if a rfs model is not already instantiated in a "set". The set notion can be "the list of service instance attached to a customer" or "the complete list of all instances in the Service Resolver inventory"
* rfs_instance_capacity_rule : the rfs will be instantiated if a rfs model is alreday instantiated and has no longer service capacity

Using "**param_in_cfs_order_rule**", you need to indicate the following 3 informations : cfs_param_name, operator, target_value

example

```json
    "rule_params": {
        "cfs_param_name": "featureLevel",
        "operator": "equal",
        "target_value": "rich"
    }
```

"operator" attribute can have one of the following value : equal, greater, less, not_equal

Using "**rfs_instance_in_a_set_rule**", you need to indicate the following 2 informations : spec_name, set_id_key

example

```json
    "rule_params": {
        "spec_name": "vFW_model_A",
        "set_id_key": "all"
    }
```

"spec_name" must be the name of a rfs_spec

"set_id_key" can be one of the following value : customer, all

"customer" value means that Service Resolver will search about rfs instance in the inventory database but only those attached to the customer name provided in the order

"all" value means that Service Resolver will search about rfs instance in the full inventory database

Using "**rfs_instance_capacity_rule**", you need to indicate the following 3 informations : spec_name, set_id_key, capacity

example

```json
    "rule_params": {
        "spec_name": "vFW_model_B",
        "set_id_key": "all",
        "capacity": 2
    }
```

"spec_name" must be the name of a rfs_spec

"set_id_key" can be one of the following value : customer, all

"customer" value means that Service Resolver will search about rfs instance in the inventory database but only those attached to the customer name provided in the order

"all" value means that Service Resolver will search about rfs instance in the full inventory database

"capacity" value must be the maximum number of services that can support the rfs

Those rules can be chained/combined using the fields :

* ruleEvaluationPriority : to sequence the rule evluations for a rfs
* is_definitive_decision : to prevent from trying to evaluate a next rule
* next_instantiationDecisionRules : to force the rule sequencing
