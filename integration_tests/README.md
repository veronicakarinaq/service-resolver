# Integration tests for Service Resolver

## Overview

This part of the project is about running integration tests
Objective is to check if Service Resolver is able to run a
complete sequence using the various part of the application.

## pre-requisite

Service Resolver must be running

"my_newman" docker image must have been built using the Dockerfile provided
in the Postman repository.

```bash
docker build -t my_newman .
```

This images is based on the official postman-newman docker image with the addition
of an html reporter that generate nice html pages.

## Running tests

To run the integration tests, please execute the following, in a terminal:

```bash
docker run --network="host" --volume="/home/user/dev/service-resolver/postman:/etc/newman" my_newman run test-sr.postman_collection.json --environment service_resolver.postman_environment.json --export-environment service_resolver.postman_environment.json --reporters cli,json,htmlextra --reporter-cli-no-assertions --reporter-cli-no-console
```

It will basically run a container with postman-newman and process a collection that will test
various operations on the various APIs.

## View results

On your console, results are displayed

in /home/user/dev/service-resolver/postman/newman there is also json and html files

Open a Web Browser, select file in the menu and open the html file located in /home/user/dev/service-resolver/postman/newman

![image1](images/integration_tests_report_01.png)
