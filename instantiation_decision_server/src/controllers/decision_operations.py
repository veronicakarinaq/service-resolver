#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""cfs order related decision operations
"""
import logging
from instantiation_decision_client_to_api import find_service, get_service

LOGGER = logging.getLogger('instantiation_decision_api')


def compare(val1, operator, val2):
    """some comparisons
    """
    if operator == "equal":
        return val1 == val2
    if operator == "greater":
        return val1 > val2
    if operator == "less":
        return val1 < val2
    if operator == "not_equal":
        return val1 != val2
    return False


def select_decision(rule_results, rule_response):
    """select decision
    """
    decision = {}
    for rule_result in rule_results:
        if rule_result["rule_response"] == "true":
            rule_result["rule_response"] = True
        if rule_result["rule_response"] == "false":
            rule_result["rule_response"] = False
        if rule_result["rule_response"] == rule_response:
            decision["decision"] = rule_result["decision"]
            final_dec = rule_result["is_definitive_decision"]
            decision["is_definitive_decision"] = final_dec
            if "next_instantiationDecisionRules" in rule_result:
                next_rule = rule_result["next_instantiationDecisionRules"]
                decision["next_instantiationDecisionRules"] = next_rule
            return decision
    decision["decision"] = "no_decision"
    decision["is_definitive_decision"] = False
    return decision


def param_in_cfs_order(order_item,
                       rule_params,
                       rule_results):
    """ instantiation decision rule for a rfs_spec, based on the value of a
        cfs characteristic, using a comparison operator
    """
    response = {}
    for param in order_item["service"]["serviceCharacteristic"]:
        if param["name"] == rule_params["cfs_param_name"]:
            comp = compare(param["value"],
                           rule_params["operator"],
                           rule_params["target_value"])
            rule_response = bool(comp)
            response["decision"] = select_decision(rule_results, rule_response)
            response["rfs"] = []
            response["related_rfs"] = []
            return response
    response["decision"] = select_decision(rule_results, "no_param")
    response["rfs"] = []
    response["related_rfs"] = []
    return response


def instance_in_a_set(order_item,
                      rule_params):
    """search for existing instance that match the spec name
       in a set of instance. set_id can be "all" or  a customer_id
    """
    response = find_service()
    instance_list = response
    return_list = []
    if rule_params["set_id_key"] == "customer_id":
        customer_id = order_item["service"]["relatedParty"][0]["id"]
        for instance in instance_list:
            spec_name = instance["serviceSpecification"]["name"]
            if spec_name == rule_params["spec_name"]:
                if instance["relatedParty"][0]["id"] == customer_id:
                    return_list.append(instance)
    if rule_params["set_id_key"] == "all":
        for instance in instance_list:
            spec_name = instance["serviceSpecification"]["name"]
            if spec_name == rule_params["spec_name"]:
                return_list.append(instance)
    return return_list


def get_related_instances(instance):
    """search for related rfs instance that have the same cfs instance
    """
    cfs = get_service(instance["serviceRelationship"][0]["id"])
    related_rfs = cfs["supportingService"]
    return related_rfs


def instance_exists_in_a_set(order_item,
                             rule_params,
                             rule_results):
    """search for existing instance that match the spec name
       in a set of instance. set_id can be "all" or  a customer_id
    """
    response = {}
    instance_list = instance_in_a_set(order_item,
                                      rule_params)
    for instance in instance_list:
        tmp = rule_params["spec_name"]
        if instance["serviceSpecification"]["name"] == tmp:
            related_instances = []
            if "collateral" in rule_params:
                related_rfs_list = get_related_instances(instance)
                related_instances = related_rfs_list.copy()
                for elem in related_rfs_list:
                    if elem["spec_name"] not in rule_params["collateral"]:
                        related_instances.remove(elem)
            response["decision"] = select_decision(rule_results, True)
            response["rfs"] = instance
            response["related_rfs"] = related_instances
            LOGGER.debug('\nrelated_rfs %s \n', related_instances)
            return response
    response["decision"] = select_decision(rule_results, False)
    response["rfs"] = []
    response["related_rfs"] = []
    return response


def rfs_instance_capacity(order_item,
                          rule_params,
                          rule_results):
    """decision_rule for rfs_instance capacity (example : a VNF that is only
       able to support XX conf/service)
       with the hypothesis that there s a "current_occupation" rfs_spec param
       if rule_response is True, it means there is capacity available and then
       no need for instantiation
       if rule_response is an empty_list or False, then there is no capacity
    """
    response = {}
    rfs_instance_list = instance_in_a_set(order_item,
                                          rule_params)
    for rfs in rfs_instance_list:
        occupation = len(rfs["serviceRelationship"])
        comp = compare(occupation, "less", rule_params["capacity"])
        if comp:
            response["decision"] = select_decision(rule_results, comp)
            response["rfs"] = rfs
            response["related_rfs"] = []
            return response
    response["decision"] = select_decision(rule_results, False)
    response["rfs"] = []
    response["related_rfs"] = []
    return response


def undefined_rule_name():
    """ in case of undefined rule_name
    """
    response = {}
    response["decision"] = "no_decision"
    response["rfs"] = []
    response["related_rfs"] = []
    return response


def perform_decision(order_item, rule_params, rule_results, rule_name):
    """execute the rule_name
    """
    if rule_name == "rfs_instance_capacity_rule":
        response = rfs_instance_capacity(order_item,
                                         rule_params,
                                         rule_results)
    elif rule_name == "instance_exists_in_a_set_rule":
        response = instance_exists_in_a_set(order_item,
                                            rule_params,
                                            rule_results)
    elif rule_name == "param_in_cfs_order_rule":
        response = param_in_cfs_order(order_item,
                                      rule_params,
                                      rule_results)
    else:
        response = undefined_rule_name()
    return response
