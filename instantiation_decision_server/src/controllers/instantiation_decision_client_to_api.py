#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""clients to other API
"""
import logging
import requests
import instantiation_decision_exceptions as service_resolver_exceptions
from instantiation_decision_utils import get_config

LOGGER = logging.getLogger('instantiation_decision_api')
SERVICE_API_BASE_URL = ("http://" +
                        get_config("service_resolver.service_api.host") +
                        ":" +
                        str(get_config("service_resolver.service_api.port")) +
                        get_config("service_resolver.service_api.url"))
SERVICE_API_HEADER = get_config("service_resolver.service_api.headers")


def get_service(service_id):
    ''' get a service by id
    '''
    LOGGER.debug('call to get_service function')

    url = (SERVICE_API_BASE_URL +
           '/service/' +
           service_id)
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    if response.status_code == 404:
        message = "not found in service API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceNotFoundException(message)
    response = response.json()
    return response


def find_service():
    ''' find service
    '''
    LOGGER.debug('call to find_service function')

    url = SERVICE_API_BASE_URL + "/service"
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=SERVICE_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with SERVICE API"
        LOGGER.error(message)
        raise service_resolver_exceptions.ServiceException(message)
    response = response.json()
    return response
