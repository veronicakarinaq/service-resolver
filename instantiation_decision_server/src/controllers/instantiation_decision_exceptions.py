#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" Module to define testing exceptions """

__author__ = ("Morgan Richomme <morgan.richomme@orange.com>")


class SoCompletionException(Exception):
    """Raise when SO request cannot be completed."""


class SoRequestException(Exception):
    """Raise when SO request cannot be executed."""


class SdcRequestException(Exception):
    """Raise when SDC request cannot be executed."""


class AaiRequestException(Exception):
    """Raise when AAI request cannot be executed."""


class VnfInitException(Exception):
    """Missing parameters in VNF definition."""


class ServiceInstantiateException(Exception):
    """Service cannot be instantiate."""


class ServiceRelationException(Exception):
    """No relation to establish for this VF."""


class VNFInstantiateException(Exception):
    """VNF cannot be instantiate."""


class VfModuleInstantiateException(Exception):
    """VF Module cannot be instantiate."""


class NetworkInstantiateException(Exception):
    """Network cannot be instantiate."""


class VspOnboardException(Exception):
    """VSP checks fail."""


class CsarDownloadException(Exception):
    """Problem to retrieve csar file during onboarding."""


class VendorNotFoundException(Exception):
    """Problem to retrieve vendor while onboarding."""


class VspNotFoundException(Exception):
    """Problem to retrieve VSP while onboarding."""


class VfcNotFoundException(Exception):
    """Problem to retrieve VFC while onboarding."""


class NbiNotFoundException(Exception):
    """data not found in ONAP NBI."""


class NbiException(Exception):
    """NBI issue."""


class SdcHeatZipException(Exception):
    """Problem managing heat zip file"""


class MongodbException(Exception):
    """Problem with mongodb communication"""


class CfsSpecException(Exception):
    """Communication problem with cfs spec api"""


class CfsSpecNotFoundException(Exception):
    """Communication problem with cfs spec api"""


class ServiceException(Exception):
    """Communication problem with cfs spec api"""


class ServiceNotFoundException(Exception):
    """Communication problem with cfs spec api"""
